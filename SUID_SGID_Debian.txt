Debian 8
==============
SUID
-----
-rwsr-xr-x 1 root root 40000 Mar 29  2015 /bin/mount
-rwsr-xr-x 1 root root 146160 Jan 28  2017 /bin/ntfs-3g
-rwsr-xr-x 1 root root 40168 May 17  2017 /bin/su
-rwsr-xr-x 1 root root 27416 Mar 29  2015 /bin/umount
-rwsr-xr-x 1 root root 30800 Jan 21  2016 /bin/fusermount
-rwsr-sr-x 1 root mail 89248 Nov 18  2017 /usr/bin/procmail
-rwsr-sr-x 1 root root 10104 Apr  1  2014 /usr/bin/X
-rwsr-xr-x 1 root root 75376 May 17  2017 /usr/bin/gpasswd
-rwsr-xr-x 1 root root 39912 May 17  2017 /usr/bin/newgrp
-rwsr-xr-x 1 root root 23184 Sep  6  2016 /usr/bin/pkexec
-rwsr-xr-x 1 root root 54192 May 17  2017 /usr/bin/passwd
-rwsr-xr-x 1 root root 44464 May 17  2017 /usr/bin/chsh
-rwsr-xr-x 1 root root 53616 May 17  2017 /usr/bin/chfn
-rwsr-xr-x 1 root root 14672 Sep  6  2016 /usr/lib/policykit-1/polkit-agent-helper-1
-rwsr-xr-x 1 root root 10104 Mar 28  2017 /usr/lib/eject/dmcrypt-get-device
-rwsr-xr-x 1 root root 464904 Nov 19  2017 /usr/lib/openssh/ssh-keysign
-rwsr-xr-x 1 root root 14200 Oct 14  2014 /usr/lib/spice-gtk/spice-client-glib-usb-acl-helper
-rwsr-xr-- 1 root messagebus 294512 Nov 21  2016 /usr/lib/dbus-1.0/dbus-daemon-launch-helper
-rwsr-xr-- 1 root dip 333560 Apr 14  2015 /usr/sbin/pppd
-rwsr-xr-x 1 root root 1031296 Feb 10  2018 /usr/sbin/exim4
-rwsr-xr-x 1 root root 90456 Aug 12  2014 /sbin/mount.nfs


SGID
-----
-rwsr-sr-x 1 root root 10104 Apr  1  2014 /usr/bin/X



Debian 10
==============
SUID
-----
-rwsr-sr-x 1 root root 14608 Aug 27 03:51 /usr/lib/xorg/Xorg.wrap
-rwsr-xr-- 1 root dip 386792 Feb 20  2020 /usr/sbin/pppd
-rwsr-xr-- 1 root messagebus 51184 Jul  5  2020 /usr/lib/dbus-1.0/dbus-daemon-launch-helper
-rwsr-xr-x 1 root root 10232 Mar 27  2017 /usr/lib/eject/dmcrypt-get-device
-rwsr-xr-x 1 root root 1181384 May 13  2020 /usr/sbin/exim4
-rwsr-xr-x 1 root root 154352 Mar 21  2019 /usr/bin/ntfs-3g
-rwsr-xr-x 1 root root 157192 Feb  2  2020 /usr/bin/sudo
-rwsr-xr-x 1 root root 18424 Sep  8  2018 /usr/lib/spice-gtk/spice-client-glib-usb-acl-helper
-rwsr-xr-x 1 root root 18888 Jan 15  2019 /usr/lib/policykit-1/polkit-agent-helper-1
-rwsr-xr-x 1 root root 23288 Jan 15  2019 /usr/bin/pkexec
-rwsr-xr-x 1 root root 34888 Jan 10  2019 /usr/bin/umount
-rwsr-xr-x 1 root root 34896 Apr 22  2020 /usr/bin/fusermount
-rwsr-xr-x 1 root root 436552 Jan 31  2020 /usr/lib/openssh/ssh-keysign
-rwsr-xr-x 1 root root 44440 Jul 27  2018 /usr/bin/newgrp
-rwsr-xr-x 1 root root 44528 Jul 27  2018 /usr/bin/chsh
-rwsr-xr-x 1 root root 51280 Jan 10  2019 /usr/bin/mount
-rwsr-xr-x 1 root root 54096 Jul 27  2018 /usr/bin/chfn
-rwsr-xr-x 1 root root 55400 Mar  6  2019 /usr/bin/bwrap
-rwsr-xr-x 1 root root 63568 Jan 10  2019 /usr/bin/su
-rwsr-xr-x 1 root root 63736 Jul 27  2018 /usr/bin/passwd
-rwsr-xr-x 1 root root 84016 Jul 27  2018 /usr/bin/gpasswd

SGID
-----
-rwsr-sr-x 1 root root 14608 Aug 27 03:51 /usr/lib/xorg/Xorg.wrap
-rwxr-sr-x 1 root root 15048 Nov 22  2019 /usr/bin/dotlock.mailutils
