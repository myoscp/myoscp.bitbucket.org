set nowrap
set lines=50 columns=150
colorscheme darkblue
set sel=inclusive
set nu
set ignorecase
