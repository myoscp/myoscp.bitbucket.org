#!/bin/bash

if (( $# < 2))
then
   echo "Usage: $0 <target URL> <small | common | big> [dirb parameters]"
   echo ""
   echo "e.g.   $0 http://$IP:7331 common -X ,.pl,.sh,.php"
   echo "e.g.   $0 https://$IP big -r"
   exit 1
fi

IP=${1}
shift
WL=${1}
shift

tmp_file=$(mktemp)

dirb ${IP} /usr/share/wordlists/dirb/${WL}.txt -w -o ${tmp_file} $* > /dev/null
grep -e "+ h" ${tmp_file} | cut -d "(" -f 1| tr -d '+ ' > ${tmp_file}x
grep -e " DIRECTORY:" ${tmp_file} | cut -d " " -f 3,4,5,6,7 >> ${tmp_file}x
sort -u ${tmp_file}x 
rm ${tmp_file} ${tmp_file}x
