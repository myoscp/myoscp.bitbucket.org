#!/bin/bash

if [ $# -lt 1 ] 
then
	echo "Usage: $0 <IP> [vuln]"
	exit 1
fi

IP=${1}
VULN=${2}

echo "========================="
echo "=        -TCP"
echo "========================="
unset sep
unset ports
nmap -T4 -sV -p- ${IP} | tee nmap_tmp.txt
for port in $(cat nmap_tmp.txt | grep " open" | cut -d "/" -f 1)
do
	ports="${ports}${sep}${port}"
	sep=","
done

if [ "vulnx" != "${VULN}x" ]
then
	cmd="nmap -T4 -A -p ${ports} ${IP}"
	echo "-------------------------------"
	echo ${cmd}
	echo "-------------------------------"
	${cmd}
else
	cmd="nmap -sV -p ${ports} --script=auth,discovery,exploit,vuln,vulners ${IP}"
	#cmd="nmap -sV -p ${ports} --script=auth,brute,discovery,exploit,vuln,vulners ${IP}"
	echo "------------------------"
	echo "-        -vuln scan"
	echo ${cmd}
	echo "------------------------"
	${cmd}
fi

echo "========================="
echo "=        -UDP"
echo "========================="
unset sep
unset ports
#nmap -sU -F ${IP} | tee nmap_tmp.txt
nmap -sUV -F ${IP} | tee nmap_tmp.txt
for port in $(cat nmap_tmp.txt | grep " open" | cut -d "/" -f 1)
do
	ports="${ports}${sep}${port}"
	sep=","
done

if [ "vulnx" != "${VULN}x" ]
then
	cmd="nmap -sUV -A -p ${ports} ${IP}"
	echo "-------------------------------"
	echo ${cmd}
	echo "-------------------------------"
	${cmd}
else
	cmd="nmap -sV -p ${ports} --script=auth,discovery,exploit,vuln,vulners ${IP}"
	#cmd="nmap -sV -p ${ports} --script=auth,brute,discovery,exploit,vuln,vulners ${IP}"
	echo "------------------------"
	echo "-        -vuln scan"
	echo ${cmd}
	echo "------------------------"
	${cmd}
fi

rm nmap_tmp.txt