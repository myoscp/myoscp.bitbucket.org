#!/bin/bash


if (( $# < 1))
then
   echo "Usage: $0 <target IP> [big]"
   exit 1
fi

IP=${1}
BIG=${2}

# nohup dirbuster &

echo "scan_all.sh: $(date) nikto running"
#nikto -ask=no -h http://${IP} -C all > nikto.txt 
nikto -ask=no -h http://${IP} > nikto.txt 
if [ "$(grep "No web server found" nikto.txt)x" != "x" ]
then
   rm nikto.txt
fi
nikto -ask=no -h https://${IP} > nikto_443.txt 
if [ "$(grep "No web server found" nikto_443.txt)x" != "x" ]
then
   rm nikto_443.txt
fi
nikto -ask=no -h ${IP} -port 8080 > nikto_8080.txt 
if [ "$(grep "No web server found" nikto_8080.txt)x" != "x" ]
then
   rm nikto_8080.txt
fi



echo "scan_all.sh: $(date) whatweb running"
#whatweb http://${IP} --no-errors > whatweb.txt
if [ "$(grep "ERROR Opening" whatweb.txt)x" != "x" ]
then
   rm whatweb.txt
fi
#whatweb https://${IP} --no-errors > whatweb_443.txt 
if [ "$(grep "ERROR Opening" whatweb_443.txt)x" != "x" ]
then
   rm whatweb_443.txt
fi
#whatweb http://${IP}:8080 --no-errors > whatweb_8080.txt
if [ "$(grep "ERROR Opening" whatweb_8080.txt)x" != "x" ]
then
   rm whatweb_8080.txt
fi


dn=$(grep "$IP" /etc/hosts | cut -d " " -f 2)

echo "scan_all.sh: $(date) wpscan running"
wpscan --update 1>/dev/null 2>&1
wpscan --url http://${IP} -e u,ap,t --detection-mode mixed --plugins-detection mixed --no-update > wpscan.txt
if [ "$(grep "supplied redirects" wpscan.txt)x" != "x" ]
then
   wpscan --url http://${dn} -e u,ap,t --detection-mode mixed --plugins-detection mixed --no-update > wpscan.txt
fi
if [ "$(grep "Scan Aborted" wpscan.txt)x" != "x" ]
then
   wpscan --url http://${IP}/wordpress/ -e u,ap,t --detection-mode mixed --plugins-detection mixed --no-update > wpscan.txt
fi
# no result -> delete
if [ "$(grep "Scan Aborted" wpscan.txt)x" != "x" ]
then
   rm wpscan.txt
fi


wpscan --url https://${IP} -e u,ap,t --detection-mode mixed --plugins-detection mixed --no-update --disable-tls-checks > wpscan_443.txt
if [ "$(grep "supplied redirects" wpscan_443.txt)x" != "x" ]
then
   wpscan --url https://${dn} -e u,ap,t --detection-mode mixed --plugins-detection mixed --no-update --disable-tls-checks > wpscan_443.txt
fi
if [ "$(grep "Scan Aborted" wpscan_443.txt)x" != "x" ]
then
   wpscan --url https://${IP}/wordpress/ -e u,ap,t --detection-mode mixed --plugins-detection mixed --no-update --disable-tls-checks > wpscan_443.txt
fi
# no result -> delete
if [ "$(grep "Scan Aborted" wpscan_443.txt)x" != "x" ]
then
   rm wpscan_443.txt
fi

echo "$(date) wordlist generation running"
cewl -d 4 -m 3 --with-numbers -e --email_file emaillist.txt -w wordlist.txt $IP
cewl -d 4 -m 3 --with-numbers -e --email_file emaillist_443.txt -w wordlist_443.txt https://${IP}:443
cat wordlist.txt | sort -u > w.txt ; mv w.txt wordlist.txt
cat wordlist_443.txt | sort -u > w.txt ; mv w.txt wordlist_443.txt
#john --wordlist=wordlist.txt --rules --stdout | sort -u > wordlist_mut.txt
#john --wordlist=wordlist_443.txt --rules --stdout | sort -u > wordlist_443_mut.txt


echo "scan_all.sh: smb - enum4linux"
enum4linux -a ${IP} > smb.txt
# sometimes still usefull
#if [ "$(grep "Can't find workgroup/domain" smb.txt)x" != "x" ]
#then
#   rm smb.txt
#fi


echo "scan_all.sh: $(date) dir busting - limited recursion depth!"
#feroxbuster -d 2 -x php,txt -w /usr/share/wordlists/custom.txt -u http://$IP -o dirb.txt
feroxbuster -d 2 -x php,txt,html -r -w /usr/share/wordlists/custom_small.txt -u http://$IP -o dirb.txt
feroxbuster -d 2 -x php,txt,html -r -w /usr/share/wordlists/custom_small.txt -u https://$IP:443 -o dirb_443.txt -k
feroxbuster -d 2 -x php,txt,html -r -w /usr/share/wordlists/custom_small.txt -u http://$IP:8080 -o dirb_8080.txt
#gobuster dir -e -k -x php,txt,html -f -w /usr/share/wordlists/custom.txt -u http://$IP | sort -u > dirb.txt
#gobuster dir -e -k -x php,txt,html -f -w /usr/share/wordlists/custom.txt -u http://$IP:443 | sort -u > dirb_443.txt


echo "scan_all.sh: $(date) nmap running"
nmap_staged.sh ${IP} > nmap.txt 

nmap_staged.sh ${IP} vuln > nmap_vuln.txt 

rm -f nohup.out 2> /dev/null

find . -size  0  -delete
