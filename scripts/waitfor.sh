#!/bin/bash

if (( $# < 1))
then
   echo "Usage: $0 <unique command string for grep>"
   echo ""
   echo "e.g.   $0 ffuf"
   exit 1
fi

while ps -ef|grep ${1}|grep -v grep|grep -v waitfor;do sleep 30; done