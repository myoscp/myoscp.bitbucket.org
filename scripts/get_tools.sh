#!/bin/bash

cd

echo "set shell to bash"
chsh -s $(which bash)

echo "set time zone"
timedatectl set-timezone Europe/Vienna

echo "set keyboard layout"
localectl set-x11-keymap de pc105

apt update
apt install gedit -y

# in the meantime - edit the configs...
gedit /etc/ssl/openssl.cnf /etc/samba/smb.conf &

wget -O pentest.zip https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/pentest.zip
unzip -n pentest.zip

wget -O zipboxes.sh https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/zipboxes.sh
chmod 755 zipboxes.sh

apt install stegosuite libimage-exiftool-perl feroxbuster ffuf nishang -y

echo "apt update && apt upgrade -y && apt autoremove -y" > update.sh
echo "openvpn ~/thm_scubid.ovpn" > connect_thm.sh
chmod 755 *.sh
./update.sh

cd /usr/local/bin

wget -O bof.py https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/bof.py
wget -O dirb_list.sh https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/dirb_list.sh
wget -O ffuf_list.sh https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/ffuf_list.sh
wget -O get_tools.sh https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/get_tools.sh
wget -O login_bf.py https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/login_bf.py
wget -O nmap_staged.sh https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/nmap_staged.sh
wget -O scan_all.sh https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/scan_all.sh
wget -O ssh_user_enum.py https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/ssh_user_enum.py
wget -O waitfor.sh https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/waitfor.sh

wget -O lse.sh https://raw.githubusercontent.com/diego-treitos/linux-smart-enumeration/master/lse.sh
wget -O linuxprivchecker.py https://raw.githubusercontent.com/sleventyeleven/linuxprivchecker/master/linuxprivchecker.py
wget -O LinEnum.sh https://raw.githubusercontent.com/rebootuser/LinEnum/master/LinEnum.sh
wget -O upc.sh https://raw.githubusercontent.com/pentestmonkey/unix-privesc-check/1_x/unix-privesc-check
wget -O linpeas.sh https://raw.githubusercontent.com/carlospolop/privilege-escalation-awesome-scripts-suite/master/linPEAS/linpeas.sh

chmod 755 *.py *.sh

wget -O peda.py https://raw.githubusercontent.com/longld/peda/master/peda.py
echo "source /usr/local/bin/peda.py" >> /etc/gdb/gdbinit

# remove 'manual' from all wordlists
# for better scan performance
cd /usr/share/wordlists/dirb
echo "delete 'manual', empty lines etc. from dirb/dirbuster wordlists"
for wordlist in $(grep -alw "manual" *.txt)
do
  dos2unix ${wordlist}

  # delete 'manual'
  sed '/^manual$/d' ${wordlist} > ${wordlist}_tmp
  mv ${wordlist}_tmp ${wordlist}
  # delete backslash \
  sed '/\\/d' ${wordlist} > ${wordlist}_tmp
  mv ${wordlist}_tmp ${wordlist}
  # delete empty lines
  sed '/^$/d' ${wordlist} > ${wordlist}_tmp
  mv ${wordlist}_tmp ${wordlist}
done

cd /usr/share/wordlists/dirbuster
for wordlist in $(grep -alw "manual" *.txt)
do
  # delete 'manual'
  sed '/^manual$/d' ${wordlist} > ${wordlist}_tmp
  mv ${wordlist}_tmp ${wordlist}
  # delete empty lines
  sed '/^$/d' ${wordlist} > ${wordlist}_tmp
  mv ${wordlist}_tmp ${wordlist}
done

# wget https://raw.githubusercontent.com/daviddias/node-dirbuster/master/lists/directory-list-2.3-big.txt
# dos2unix directory-list-2.3-big.txt

cd /usr/share/wordlists
gunzip rockyou.txt.gz
# delete empty lines
sed '/^$/d' rockyou.txt > rockyou.txt_tmp
mv rockyou.txt_tmp rockyou.txt

head -n 50000 rockyou.txt > rockyou_50000.txt
gzip rockyou.txt

# generate custom wordlist for dirbusting
echo "generate custom wordlists"
cat /usr/share/wordlists/dirb/small.txt /usr/share/wordlists/dirb/common.txt /usr/share/wordlists/dirb/big.txt /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt | grep -v " " | grep -v "^\.ht" | sort -u > ./custom.txt
cat /usr/share/wordlists/dirb/small.txt /usr/share/wordlists/dirb/common.txt /usr/share/wordlists/dirb/big.txt | grep -v " " | grep -v "^\.ht" | sort -u > ./custom_small.txt
cat /usr/share/wordlists/dirb/small.txt /usr/share/wordlists/dirb/common.txt | grep -v " " | grep -v "^\.ht" | sort -u > ./custom_very_small.txt

# generate names list
#echo "generate name list"
#cat /usr/share/wordlists/nmap.lst /usr/share/wordlists/wfuzz/others/names.txt /usr/share/wordlists/dirb/others/names.txt | tr '[:upper:]' '[:lower:]' | sed '/^$/d' | sed '/^[0-9]*$/d' | grep -v "comment:" | sort -u > ./names.txt
# better use /usr/share/seclists/Usernames/Names/names.txt

wget -O sqli.txt https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/sqli.txt
wget -O sqli_time.txt https://bitbucket.org/myoscp/myoscp.bitbucket.org/raw/HEAD/scripts/sqli_time.txt

apt install seclists -y
ln -s /usr/share/seclists /usr/share/wordlists/seclists

cd /opt
git clone https://github.com/Dionach/CMSmap.git
#cd CMSmap
#./cmsmap.py http://localhost &

echo "alias hgrep='history|grep'" >> ~/.bashrc

updatedb