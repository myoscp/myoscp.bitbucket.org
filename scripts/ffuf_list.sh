#!/bin/bash

tmp_file=$(mktemp)

ffuf $* -of csv -o ${tmp_file} 1>&2
cat ${tmp_file} | tr ',' '\t' | cut -d$'\t' -f 2,5 | sort -u | grep -v "status_code"
rm ${tmp_file}
